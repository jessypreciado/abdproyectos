create database CLUB_AJEDREZ;
use CLUB_AJEDREZ;
/*Creacion de tablas*/

create table paises(id_pais int  auto_increment primary key,
nombre varchar(25),
cantclub int,
id_paisr varchar(10)
);
SELECT * FROM paises;
show tables ;
/**/
create table hoteles( id_hotel int primary key auto_increment,
nombre varchar(15),
direccion varchar(30),
telefono varchar(10));

create table salas(id_sala int  auto_increment primary key ,
capacidad int,
medios varchar(30),
id_hotel int,
foreign  key(id_hotel) references hoteles(id_hotel));

drop table salas;


create table reservaciones(id_reservacion int primary key auto_increment,
fecha_entrada varchar(30),
fecha_salida varchar(30),
id_hotel int,
id_participante varchar (40),
foreign key(id_hotel) references hoteles(id_hotel),
foreign key(id_participante)references participantes(id_participante));

drop table reservaciones;


create table participantes(id int primary key auto_increment, id_participante varchar(60) ,
nombre varchar(20),
direccion varchar(30),
telefono varchar(10),
tipo int,
id_pais int,
niveljuego int,
foreign key (id_pais) references paises(id_pais));

drop table participantes;
insert into participantes values (null ,'1','jose','3 de mayo N16','474735876',1,3,1);
insert into participantes values (null ,'jesica','lagos de moreno','254896122',1,1,6);

insert into participantes values (null ,'carlos','leon','7425623',2,2);
insert into participantes values (null ,'adriana','lagos de moreno','474539682',2,3);

select *from participantes;

create table jugadores (id_registro int primary key  auto_increment,id_jugador varchar(20) ,
foreign key (id_jugador) references participantes(id_participante));

insert into jugadores values (null);

insert into jugadores values (1);
select *from jugadores;
show tables;
select *from jugadores;
drop table jugadores;

create table arbitros (id_a int primary key auto_increment,id_arbitro int ,
foreign key(id_arbitro) references participantes(id_participante));
insert into arbitros values(3);
insert into arbitros values(4);
select *from arbitros;
drop table arbitros ;
create table movimientos(id_movimiento int primary key auto_increment,nombre varchar(20));


create table partidas(id int primary key auto_increment ,id_partida int ,id_movimiento int ,id_arbitro int ,id_sala int ,
foreign key (id_sala) references salas(id_sala),
foreign key (id_movimiento) references movimientos(id_movimiento),
foreign key (id_arbitro) references arbitros(id_arbitro)
);






SELECT *FROM PARTIDAS;

/* nombre y referencia jugadores*/
create table jugadas (id_jugada int primary key AUTO_INCREMENT,
id_jugador int, 
color varchar(20),
id_partida int,
foreign key (id_partida) references partidas(id_partida),
foreign key (id_jugador) references jugadores(id_jugador));

show tables;
show tables ;

CREATE USER 'clubAjedrez'@'localhost' identified by '@Ajedrez';
Grant all  on CLUB_AJEDREZ  to 'clubAjedrez'@'localhost';
flush privileges;





















