/* PROCEDURE PARTIDAS*/

create procedure ProcedurePartidaInsertar(
in Pid int,
in Pid_partida int,
in Pid_movimiento int,
IN Pid_arbitro INT,
IN Pid_sala int)
begin 
Declare num int;
set num= (select count(*)from partidas where id=Pid);
if num=0 then
   insert into partidas values (null,Pid_partida,Pid_movimiento,Pid_arbitro,Pid_sala);
else
  select "lA PARTIDA YA FUE INGRESADA";
end if;
end;

drop procedure  ProcedurePartidaInsertar;
call ProcedurePartidaInsertar(null,1,1,2,1);
call ProcedurePartidaInsertar(null, 5,2,2);
call ProcedurePartidaInsertar(null, 9,5,2);
select *from partidas;

create Procedure ProcedurePartidasEliminar(
in Pid_partida INT )
begin
Declare num int;
set num= (select count(*)from partidas where id_partida=Pid_partida);

if num=1 then

delete from partidas where id_partida=Pid_partida;
else
select "la partida no  existe";
end if;
end;
call ProcedurePartidasEliminar(1);

create Procedure ProcedurePartidaActualizar(
in Pid int,
in Pid_partida int,
in Pid_movimiento int,
in Pid_arbitro int,
in Pid_sala int)
begin
Declare num int;
set num= (select count(*)from partidas where id=Pid);
if num=1 then
update partidas set id_partida=Pid_partida, id_movimiento=Pid_movimiento,id_arbitro=Pid_arbitro,id_sala=Pid_sala where id=Pid;
else
select "la partida no existe";

end if;
end;

drop Procedure ProcedurePartidaActualizar;
call ProcedurePartidaActualizar(1,2,8,5,5);
call ProcedurePartidaActualizar(1,5,8,8);

select *from partidas;
select *from participantes;




Create PROCEDURE ProcedurePartidasMostrar(
in Pnombre  varchar(15))
begin 

select * from VPartida 
where 1=1
and 
(case when Pnombre <> '' then 
ARBITRO like concat ('%',Pnombre,'%') 
else 
1=1
end 
);
end;

drop procedure ProcedurePartidasMostrar;
CREATE VIEW VPartida 
as
select t1.id ,t1.id_partida  as 'PARTIDA' , t2.nombre as 'MOVIMIENTO', t4.id_sala AS 'ID SALA', t5.nombre as ' ARBITRO' 
from partidas t1 inner join movimientos t2
on t1.id_movimiento= t2.id_movimiento

inner join salas t4 on t1.id_sala = t4.id_sala
inner join participantes t5 on t1.id_arbitro=t5.id_participante ;

drop view VPartida;

SELECT * FROM PARTIDAS;


select * from salas;
call  ProcedurePartidasMostrar("");

select * from salas;

/* PROCEDURE JUGADAS*/
create procedure ProcedureJugadasInsertar(
in Pid_jugada int,
in  Pid_jugador int,
in Pcolor varchar(50),
in Pid_partida int)
begin 
Declare num int;
set num= (select count(*)from jugadas where id_jugada=Pid_jugada);
if num=0 then
   insert into jugadas values (Pid_jugada ,Pid_jugador,Pcolor,Pid_partida);
else
  select "lA JUGADA YA FUE INGRESADA";
end if;
end;

Call ProcedureJugadasInsertar(null,1,'azul',1);
Call ProcedureJugadasInsertar(null,2,'verde',1);
Call ProcedureJugadasInsertar(null,2,'rojo',1);

select *from PARTIDAS;

create Procedure ProcedureJugadasEliminar(
in Pid_jugada INT )
begin
Declare num int;
set num= (select count(*)from jugadas where id_jugada=Pid_jugada);
if num=1 then

delete from jugadas where id_jugada=Pid_jugada;
else
select "la jugada no  existe";
end if;
end;
drop procedure ProcedureJugadasEliminar; 
call ProcedureJugadasEliminar(1);
select *from jugadas;

create Procedure ProcedureJugadaActualizar(
in Pid_jugada int,
in  Pid_jugador int,
in Pcolor varchar(50),
in Pid_partida int)
begin
Declare num int;
set num= (select count(*)from jugadas where id_jugada=Pid_jugada);

if num=1 then
update jugadas set id_jugada=Pid_jugada,id_jugador=Pid_jugador,color=Pcolor, id_partida=Pid_partida
where id_jugada=Pid_jugada;
else
select "la jugada no existe";

end if;
end;
drop procedure ProcedureJugadaActualizar ;
call ProcedureJugadaActualizar (2,3,'cafe',2);
select *from jugadas;


Create PROCEDURE ProcedureJugadasMostrar(
in Pnombre varchar(20))
begin
select * from Vjugadas
where 1=1
and
(case when Pnombre <> '' then
JUGADOR like concat ('%',Pnombre,'%')
else
1=1
end
);
end;
drop procedure ProcedureJugadasMostrar;
call ProcedureJugadasMostrar("");
SELECT *FROM jugadas;
select *from jugadores;
select *from partidas ;

CREATE VIEW Vjugadas as select t1.id_jugada as 'JUGADA', t4.nombre AS 'JUGADOR', t1.color AS 'COLOR' , t3.id_partida AS 'PARTIDA' from jugadas t1 
INNER JOIN jugadores t2 on t1.id_jugador = t2.id_jugador 
inner join partidas t3 on t1.id_partida =t3.id_partida
inner join participantes t4 on t4.id_participante =t1.id_jugador;


/* PROCEDURE RECERVACIONES*/
create procedure ProcedureReservacionesInsertar(
in Pid_reservacion int,
in Pfecha_entrada varchar(50),
in Pfecha_salida varchar(50),
in Pid_hotel varchar(50),
in Pid_participante varchar(50)
)
begin 
Declare num int;
set num= (select count(*)from reservaciones where id_reservacion=Pid_reservacion);
if num=0 then
   insert into reservaciones values (Pid_Reservacion,Pfecha_entrada,Pfecha_salida,Pid_hotel,Pid_participante);
else
  select "EL ARBITRO YA FUE INGRESADA";
end if;
end;
drop procedure ProcedureReservacionesInsertar;
CALL ProcedureReservacionesInsertar(null,'02-02-2016','08-03-2016',1,1);
CALL ProcedureReservacionesInsertar(null,'05-02-2015','08-05-2015',1,2);
CALL ProcedureReservacionesInsertar(null,'05-02-2015','08-05-2015',2,3);
select *from reservaciones;


create Procedure ProcedureReservacionesEliminar(
in Pid_reservacion INT )
begin
Declare num int;
set num= (select count(*)from reservaciones where id_reservacion=Pid_reservacion);
if num=1 then

delete from reservaciones where id_Reservacion=Pid_Reservacion;
else
select "el arbitro no  existe";
end if;
end;
drop procedure ProcedureReservacionesEliminar; 
call ProcedureReservacionesEliminar(3);

create Procedure ProcedurReservacionActualizar(
in Pid_reservacion int,
in Pfecha_entrada varchar(50),
in Pfecha_salida varchar(50),
in Pid_hotel varchar(50),
in Pid_participante varchar(50))
begin
Declare num int;
set num= (select count(*)from reservaciones where id_reservacion=Pid_reservacion);

if num=1 then
update reservaciones 
set id_reservacion=Pid_reservacion,fecha_entrada=Pfecha_entrada,fecha_salida=Pfecha_salida,
    id_hotel=Pid_hotel,id_participante =Pid_participante
where id_reservacion=Pid_reservacion;
else
select "el arbitro no existe";

end if;
end;
drop procedure ProcedurReservacionActualizar ;
call ProcedurReservacionActualizar(1,'05-02-2017','08-05-2017',2,1);
select *from reservaciones;

Create PROCEDURE ProcedureReservacionMostrar(
in PHotel varchar(20))
begin 
select * from  vreservacion
where 1=1
and 
(case when PHotel <> '' then 
HOTEL like concat ('%',PHotel,'%') 
else 
1=1
end 
);
end;
drop procedure ProcedureReservacionMostrar;
call ProcedureReservacionMostrar("");
select * from reservaciones;

create view vreservacion
as
select t1.id_reservacion as 'RESERVACION' ,t1.id_participante,t3.nombre as 'participante',t1.fecha_entrada as 'FEHA ENTRADA', t1.fecha_salida as 'FECHA SALIDA',
t2.nombre as 'HOTEL',t2.id_hotel 
from reservaciones t1 inner join hoteles t2 ON t1.id_hotel =t2.id_hotel inner join participantes t3 on t1.id_participante=t3.id_participante;

select * from  vreservacion;


/*PROCEDURES PAISES */
create procedure ProcedurePaisInsertar(
in Pid_pais int,
in Pnombre varchar(25),
in Pcantclub int,
in Pid_paisr varchar(25)
)
begin
Declare num int;
set num= (select count(*)from paises where nombre= Pnombre);
if num=0 then
   insert into paises values (Pid_pais,Pnombre,Pcantclub,Pid_paisr);
else
  select "el pais ya fue ingresado";
end if;
end;



call ProcedurePaisInsertar(null,'EUA',1,2);
call ProcedurePaisInsertar(null,'Mexico',2,3);
call ProcedurePaisInsertar(null,'Colombia',6,4);


create Procedure ProcedurePaisEliminar(
in Pid_pais INT )
begin
Declare num int;
set num= (select count(*)from paises where id_pais=Pid_pais);

if num=1 then

delete from paises where id_pais=Pid_pais;
else
select "el pais no existe";

end if;
end;
call ProcedurePaisEliminar(2);


create Procedure ProcedurePaisActualizar(
in Pid_pais int,
in Pnombre varchar(25),
in Pcantclub int,
IN Pid_paisr varchar(25))

begin
Declare num int;
set num= (select count(*)from paises where id_pais=Pid_pais);

if num=1 then
update paises set nombre=Pnombre,cantclub=Pcantclub,id_paisr=Pid_paisr where id_pais=Pid_pais;
else
select "el pais no existe";

end if;
end;

drop procedure ProcedurePaisActualizar;

call ProcedurePaisActualizar(3,'CHINA',12,1);

Create PROCEDURE ProcedurePaisesMostrar(
in Pid_pais int, 
in Pnombre varchar(30))
begin
SELECT  id_pais as 'ID',nombre as 'Nombre',cantclub as 'Numero de clubs',nombre  as 'Representa'   from paises  
 where 1=1
and 
(case
when Pid_pais >0 then id_pais=Pid_pais
else
1=1
end )
and 
(case when Pnombre <> '' then 
nombre like concat ('%',Pnombre,'%') 
else 
1=1
end 
);
end;

call  ProcedurePaisesMostrar(null,'');
call  ProcedurePaisesMostrar(null,'COL');
call  ProcedurePaisesMostrar(null,'A');
SELECT *FROM PAISES;
SELECT *FROM PAISES;
SELECT *FROM PAISES;
SELECT *FROM PAISES;


/*PROCEDURES MOVIMIENTOS */
create procedure ProcedureMovimientosInsertar(
in Pid_movimiento int,
in Pnombre varchar(25)
)
begin
Declare num int;
set num= (select count(*)from  movimientos where nombre= Pnombre);
if num=0 then
   insert into movimientos values (Pid_movimiento,Pnombre);
else
  select "el movimiento ya fue ingresado";
end if;
end;
call ProcedureMovimientosInsertar(null,'ALFILES');
call ProcedureMovimientosInsertar(null,'REY');
call ProcedureMovimientosInsertar(null,'REYNA');



create Procedure ProcedureMovimientosEliminar(
in Pid_movimiento INT )
begin
Declare num int;
set num= (select count(*)from movimientos where id_movimiento=Pid_movimiento);
if num=1 then
  delete from movimientos where id_movimiento=Pid_movimiento;
 else
  select "el movimiento no existe";
end if;
end;
call ProceduremovimientosEliminar(6);



create Procedure ProcedureMovimientosActualizar(
in Pid_movimiento int,
in Pnombre varchar(25))
begin
Declare num int;
set num= (select count(*)from  movimientos where id_movimiento= Pid_movimiento);

if num=1 then
update movimientos set nombre=Pnombre where id_movimiento=Pid_movimiento;
else
select "el movimiento no existe";

end if;
end;
call proceduremovimientosActualizar(1,'ALFIL');
call proceduremovimientosActualizar(2,'REYNA');
call proceduremovimientosActualizar(3,'CABALLO');




Create PROCEDURE ProcedureMovimientosMostrar(
in Pid_movimientos int, 
in Pnombre varchar(30))
begin
SELECT  id_movimiento as 'ID',nombre as 'Nombre' from movimientos
where 1=1
and 
(case
when Pid_movimientos >0 then id_movimiento=Pid_movimientos
else
1=1
end )
and 
(case when Pnombre <> '' then 
nombre like concat ('%',Pnombre,'%') 
else 
1=1
end 
);
end;
call  ProcedureMovimientosMostrar(null,'REYNA');
call  ProcedureMovimientosMostrar(null,'REY');
call  ProcedureMovimientosMostrar(null,'ALFIL');


/*PROCEDURE HOTELES*/
create procedure ProceduresHotelesInsertar(
in Pid_id_hotel int,
in Pnombre varchar(25),
in Pdireccion varchar(30),
in Ptelefono varchar(10)
)
begin
Declare num int;
set num= (select count(*)from  hoteles where nombre= Pnombre);
if num=0 then
   insert into hoteles values (Pid_id_hotel,Pnombre,Pdireccion, Ptelefono);
else
  select "el Hotel ya fue ingresado";
end if;
end;
call ProceduresHotelesInsertar(null,'ATLANTIS','EUA','475152');
call ProceduresHotelesInsertar(null,'LAS VEGAS','EUA','8596116');
call ProceduresHotelesInsertar(null,'VICTORIA','MEXICO','5263616');
SELECT * FROM SALAS;



create Procedure ProcedureHotelesEliminar(
in Pid_id_hotel INT )
begin
Declare num int;
set num= (select count(*)from Hoteles where id_hotel=Pid_id_hotel);
if num=1 then
  delete from hoteles where id_hotel=Pid_id_hotel;
 else
  select "el hotel no existe";
end if;
end;
call ProcedureHotelesEliminar(3);



create Procedure ProcedureHotelesActualizar(
in Pid_id_hotel int,
in Pnombre varchar(25),
in Pdireccion varchar(30),
in Ptelefono varchar(10)
)
begin
Declare num int;
set num= (select count(*)from hoteles where id_hotel= Pid_id_hotel);

if num=1 then
update hoteles set nombre=Pnombre ,direccion=Pdireccion,telefono=Ptelefono  where id_hotel=Pid_id_hotel;
else
select "el hotel no existe";

end if;
end;

call ProcedureHotelesActualizar(1,'ATLANTIS2','EUA','475152');
call ProcedureHotelesActualizar(2,'LAS VEGAS 2','EUA','5693316');
call ProcedureHotelesActualizar(2,'LAS VEGAS ','EUA','5693316');



Create PROCEDURE ProcedureHotelesMostrar(
in Pid_hotel int, 
in Pnombre varchar(30))
begin
SELECT  id_hotel as 'ID',nombre as 'Nombre' from hoteles
where 1=1
and 
(case
when Pid_hotel >0 then id_hotel=Pid_hotel
else
1=1
end )
and 
(case when Pnombre <> '' then 
nombre like concat ('%',Pnombre,'%') 
else 
1=1
end 
);
end;
call  ProcedureHotelesMostrar(null,'VEGAS');
call  ProcedureHotelesMostrar(null,'ATLA');
call  ProcedureHotelesMostrar(null,'LAS');



/*PROCEDURES SALAS*/
create procedure ProcedureSalaInsertar(
in Pid_sala int,
in Pcapacidad int,
in Pmedios varchar(30),
in Pid_hotel int
)
begin
Declare num int;
set num= (select count(*)from  salas where id_sala= Pid_sala);
if num=0 then
   insert into salas values (Pid_sala,Pcapacidad,Pmedios,Pid_hotel);
else
  select "la sala ya fue ingresado";
end if;
end;
DROP PROCEDURE ProcedureSalaInsertar;
call ProcedureSalaInsertar(null,25,'TV,BOCINAS',1);
call ProcedureSalaInsertar(null,10,'BOCINAS,PROYECTOR',2);
call ProcedureSalaInsertar(null,12,'PROYECTOR',3);
SELECT *FROM Salas;

/**/
drop Procedure ProcedureSalasEliminar;
create Procedure ProcedureSalasEliminar(
in Pid_sala int
)
begin
Declare num int;
set num= (select count(*)from salas where id_Sala=Pid_sala);
if num=1 then
  delete from salas where id_Sala=Pid_sala;
 else
  select "la sala no existe";
end if;
end;
call ProcedureSalasEliminar(2);
select *from salas;
/**/
create Procedure ProcedureSalasActualizar(
in Pid_sala int,
in Pcapacidad int,
in Pmedios varchar(30),
in Pid_hotel int
)
begin
Declare num int;
set num= (select count(*)from  salas where id_sala= Pid_sala);
if num=1 then
update salas set capacidad=Pcapacidad ,medios=Pmedios,id_hotel=Pid_hotel  where id_sala=Pid_sala;
else
select "la sala no existe";

end if;
end;



drop Procedure ProcedureSalasActualizar;
call ProcedureSalasActualizar(3,25,'TV,BOCINAS',1);
call ProcedureSalasActualizar(1,11,'BOCINas',2);
call ProcedureSalasActualizar(2,12,'PROYECTOR',1);
SELECT *FROM Salas;

create Procedure ProcedureSalasMostrar(

in Pnombre varchar(20))
begin
select t1.*, t2.nombre from salas t1 left outer join hoteles t2 on t1.id_hotel=t2.id_hotel 
where 1=1
and  
(case when Pnombre <> '' then 
nombre like concat ('%',Pnombre,'%') 
else 
1=1
end 
);
end;
drop procedure ProcedureSalasMostrar;

call  ProcedureSalasMostrar('');
call  ProcedureSalasMostrar(null,'ATLA');
call  ProcedureSalasMostrar(null,'LAS');

/*PROCEDURE PARTICIPANTE*/
create procedure ProcedureParticipanteJugInsertar(
in Pid_registro int,
in Pid_participante varchar(79),
in Pnombre varchar(30),
in Pdireccion varchar(30),
in Ptelefono varchar(10),
in Ptipo int,
in Pidpais int,
in Pnivel int
)
begin
Declare num ,i int;

set num= (select count(*)from  participantes where id_participante= Pid_participante);
if num=0 then
   insert into participantes  values (Pid_registro ,Pid_participante,Pnombre,Pdireccion,Ptelefono,Ptipo,Pidpais,Pnivel);
   insert into jugadores values (null,Pid_participante);
else
  select "el participante  ya fue ingresado";
end if;
end;

call ProcedureParticipanteJugInsertar(null,'2','Joel','lagos','12639',2,5,8);

drop procedure ProcedureParticipanteJugInsertar;

select * from participantes;
select * from jugadores;




drop table participantes;



create procedure ProcedureParticianteJugEliminar(
in Pid_participante varchar (70))
begin
Declare num int;

set num= (select count(*)from  participantes where id_participante=Pid_participante );
if num=1 then
  delete from participantes  where id_participante=Pid_participante;
  delete from jugadores where id_jugador=Pid_participante;
 else
  select "El jugador no existe";
end if;
end;
call ProcedureParticianteJugEliminar('2');

drop procedure ProcedureParticianteJugEliminar;

create procedure ProcedureParticipanteJugActualizar(

in Pid_participante varchar(79),
in Pnombre varchar(30),
in Pdireccion varchar(30),
in Ptelefono varchar(10),
in Ptipo int,
in Ppais int,
in Pnivel int
)
begin
Declare num int;
set num= (select count(*)from  participantes where id_participante=Pid_participante);
if num=1 then
update participantes set nombre=Pnombre , direccion= Pdireccion, telefono =Ptelefono, tipo =Ptipo , id_pais= Ppais,niveljuego=Pnivel where id_participante=Pid_participante;
else
select "la sala no existe";
end if;
end;
drop procedure  ProcedureParticipanteJugActualizar;
call ProcedureParticipanteJugActualizar('2','jesy','5 de mayo','41452336',1,1,2);
select * from participantes;
select * from jugadores;


Create PROCEDURE ProcedureJugMostrar(
in Pnombre varchar(20))
begin 
select * from  Vjugadores
where 1=1
and 
(case when Pnombre <> '' then 
nombre like concat ('%',Pnombre,'%') 
else 
1=1
end 
);
end;

call ProcedureJugMostrar("");
drop PROCEDURE ProcedureJugMostrar;

CREATE view    Vjugadores  as 
select  t1.id , t1.id_participante ,t1.nombre, t1.direccion , t1.telefono, t1. niveljuego ,t2.nombre  as' pais' from participantes t1 inner join paises t2 on  t1.id_pais=t2.id_pais where t1.tipo=1;
select * from Vjugadores  ;






SELECT *FROM participantes;


SELECT *FROM participantes;
select *from jugadores;

create procedure ProcedureParticipanteArbInsertar(
in pid_a int,
in Pid_participante varchar(79),
in Pnombre varchar(30),
in Pdireccion varchar(30),
in Ptelefono varchar(10),
in Ptipo int,
in Pidpais int,
in Pnivel int
)
begin
Declare num int;
set num= (select count(*)from participantes where id_participante= Pid_participante);
if num=0 then insert into participantes values
(pid_a, Pid_participante,Pnombre,Pdireccion,Ptelefono,Ptipo,Pidpais,Pnivel);
insert into arbitros values (null, Pid_participante);
else
select "el participante ya fue ingresado";
end if;
end;
drop procedure ProcedureParticipanteArbInsertar;
call ProcedureParticipanteArbInsertar(null,'9','Adriana','lagos','74253',2,1,0);
call ProcedureParticipanteArbInsertar(null,'Carlos','SanJlagos','69589',2);
call ProcedureParticipanteArbInsertar(null,'Adriana','Colombia','74253',3);
select *from arbitros;
select *from participantes;

SElect * from salas;

create procedure ProcedureParticianteArbEliminar(
in Pid_participante varchar (40))
begin
Declare num int;
set num= (select count(*)from participantes where
id_participante=Pid_participante);
if num=1 then
delete from participantes where id_participante=Pid_participante;
delete from arbitros where id_arbitro=Pid_participante;
else
select "el participante no existe";
end if;
end;
call ProcedureParticianteArbEliminar('9');
drop procedure ProcedureParticianteArbEliminar;


create procedure ProcedureParticipanteArbActualizar(
in Pid int , 
in Pid_participante varchar(30),
in Pnombre varchar(30),
in Pdireccion varchar(30),
in Ptelefono varchar(10),
in Ptipo int,
in Pidpais int
)
begin
Declare num int;
set num= (select count(*)from participantes where id= Pid
);
if num=1 then
update participantes set nombre=Pnombre,direccion=Pdireccion,telefono=Ptelefono,id_tipo=Ptipo, id_pais= Pidpais  where  id= Pid;
else
select "el participante no existe";
end if;
end;
drop procedure ProcedureParticipanteArbActualizar;
call ProcedureParticipanteArbActualizar(5,'9','Julian','Mexico','12546255',2,1);
call ProcedureParticipanteJugActualizar(1,'Jose','Jalisco','2563255',2);
call ProcedureParticipanteJugActualizar(1,'Marco','Encarnacion de Diaz','52632',2);
select *from participantes;
select *from arbitros;


create Procedure ProcedureParticipanteArbMostrar(

in Pnombre varchar(20))
begin
select * from  VistaArbitro
where 1=1
and
(case when Pnombre <> '' then
nombre like concat ('%',Pnombre,'%')
else
1=1
end
);
end;

CREATE view    VistaArbitro as 
select t1.id ,t1.id_participante , t1.nombre, t1.direccion , t1.telefono ,t2.nombre as' pais' from participantes t1 inner join paises t2 on  t1.id_pais=t2.id_pais where t1.tipo!=1;


select *from participantes;
drop  Procedure ProcedureParticipanteArbMostrar;
call ProcedureParticipanteArbMostrar('');
call ProcedureParticipanteArbMostrar(2,' ');
call ProcedureParticipanteArbMostrar(null,' ');


call ProcedureReservacionMostrar("");
select * from reservaciones;

create view vjug
as
select t1.id_reservacion as 'RESERVACION' ,t1.id_participante,t3.nombre as 'participante',t1.fecha_entrada as 'FEHA ENTRADA', t1.fecha_salida as 'FECHA SALIDA',
t2.nombre as 'HOTEL',t2.id_hotel 
from reservaciones t1 inner join hoteles t2 ON t1.id_hotel =t2.id_hotel inner join participantes t3 on t1.id_participante=t3.id_participante;

select * from  vreservacion;






































/*CREATE TRIGGER tonto_el_que_lo_lea
AFTER INSERT ON Imagen
FOR EACH ROW
BEGIN
    UPDATE Album
    SET fecha_actualizacion = NOW()
    WHERE id= NEW.id_album
END
 */







