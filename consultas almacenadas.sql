

use ejemplojoin
create table productom (
idproducto int primary key,
nombre varchar(20)
);
drop table productom;
insert into productom values
(1,'Coca Cola'),
(2,'Cerveza Cronona'),
(3,'Cereal'),
(4,'Roles glaseados');
select * from productom;


/*CONSULTA DINAMICA*/
set @id=0; /*verdadero*/
set @nombre ='ce';
select * from productom where 1=1
and 
(case
when @id>0 then idproducto=@id
else 
1=1 /*verdader*/
end)
and 
(case
when @nombre <> '' then
nombre like CONCAT('%',@nombre,'%')
else 
1=1
end 
);

/*PROCEDURE*/
create procedure Proc_producto_listado(
 in p_idproducto int,
 in p_nombre varchar(20))
 
begin
select * from productom where 1=1
and 
(case
when p_idproducto>0 then idproducto=p_idproducto
else 
1=1 /*verdader*/
end)
and 
(case
when p_nombre <> '' then
nombre like CONCAT('%',p_nombre,'%')
else 
1=1
end
);
end;
call Proc_producto_listado(2,'');

drop procedure Proc_producto_listado;












