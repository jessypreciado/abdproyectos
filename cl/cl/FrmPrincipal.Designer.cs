﻿namespace cl
{
    partial class FrmPrincipal
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmPrincipal));
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.TXT_MOVIMIENTOS = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.TXT_SALAS = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.txt_arbitro = new System.Windows.Forms.Label();
            this.lbl_partida = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBox1
            // 
            this.pictureBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(0, 0);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(800, 450);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Click += new System.EventHandler(this.pictureBox1_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.SteelBlue;
            this.label1.Font = new System.Drawing.Font("Berlin Sans FB", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(73, 169);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(76, 18);
            this.label1.TabIndex = 1;
            this.label1.Text = "HOTELES ";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // TXT_MOVIMIENTOS
            // 
            this.TXT_MOVIMIENTOS.AutoSize = true;
            this.TXT_MOVIMIENTOS.BackColor = System.Drawing.Color.SteelBlue;
            this.TXT_MOVIMIENTOS.Font = new System.Drawing.Font("Berlin Sans FB", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TXT_MOVIMIENTOS.Location = new System.Drawing.Point(431, 150);
            this.TXT_MOVIMIENTOS.Name = "TXT_MOVIMIENTOS";
            this.TXT_MOVIMIENTOS.Size = new System.Drawing.Size(105, 18);
            this.TXT_MOVIMIENTOS.TabIndex = 2;
            this.TXT_MOVIMIENTOS.Text = "MOVIMIENTOS";
            this.TXT_MOVIMIENTOS.Click += new System.EventHandler(this.TXT_MOVIMIENTOS_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.SteelBlue;
            this.label2.Font = new System.Drawing.Font("Berlin Sans FB", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(252, 150);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(55, 18);
            this.label2.TabIndex = 3;
            this.label2.Text = "PAISES";
            this.label2.Click += new System.EventHandler(this.label2_Click);
            // 
            // TXT_SALAS
            // 
            this.TXT_SALAS.AutoSize = true;
            this.TXT_SALAS.BackColor = System.Drawing.Color.SteelBlue;
            this.TXT_SALAS.Font = new System.Drawing.Font("Berlin Sans FB", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TXT_SALAS.Location = new System.Drawing.Point(83, 218);
            this.TXT_SALAS.Name = "TXT_SALAS";
            this.TXT_SALAS.Size = new System.Drawing.Size(52, 18);
            this.TXT_SALAS.TabIndex = 4;
            this.TXT_SALAS.Text = "SALAS";
            this.TXT_SALAS.Click += new System.EventHandler(this.TXT_SALAS_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.SteelBlue;
            this.label3.Font = new System.Drawing.Font("Berlin Sans FB", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(55, 264);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(125, 18);
            this.label3.TabIndex = 5;
            this.label3.Text = "RESERVACIONES";
            this.label3.Click += new System.EventHandler(this.label3_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.SteelBlue;
            this.label4.Font = new System.Drawing.Font("Berlin Sans FB", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(252, 185);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(94, 18);
            this.label4.TabIndex = 6;
            this.label4.Text = "JUGADORES";
            this.label4.Click += new System.EventHandler(this.label4_Click);
            // 
            // txt_arbitro
            // 
            this.txt_arbitro.AutoSize = true;
            this.txt_arbitro.BackColor = System.Drawing.Color.SteelBlue;
            this.txt_arbitro.Font = new System.Drawing.Font("Berlin Sans FB", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_arbitro.Location = new System.Drawing.Point(255, 222);
            this.txt_arbitro.Name = "txt_arbitro";
            this.txt_arbitro.Size = new System.Drawing.Size(79, 18);
            this.txt_arbitro.TabIndex = 7;
            this.txt_arbitro.Text = "ARBITROS";
            this.txt_arbitro.Click += new System.EventHandler(this.txt_arbitro_Click);
            // 
            // lbl_partida
            // 
            this.lbl_partida.AutoSize = true;
            this.lbl_partida.BackColor = System.Drawing.Color.SteelBlue;
            this.lbl_partida.Font = new System.Drawing.Font("Berlin Sans FB", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_partida.Location = new System.Drawing.Point(434, 185);
            this.lbl_partida.Name = "lbl_partida";
            this.lbl_partida.Size = new System.Drawing.Size(72, 18);
            this.lbl_partida.TabIndex = 8;
            this.lbl_partida.Text = "PARTIDA";
            this.lbl_partida.Click += new System.EventHandler(this.lbl_partida_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.SteelBlue;
            this.label5.Font = new System.Drawing.Font("Berlin Sans FB", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(437, 226);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(67, 18);
            this.label5.TabIndex = 9;
            this.label5.Text = "JUGADA";
            this.label5.Click += new System.EventHandler(this.label5_Click);
            // 
            // FrmPrincipal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(206)))), ((int)(((byte)(227)))), ((int)(((byte)(246)))));
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.lbl_partida);
            this.Controls.Add(this.txt_arbitro);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.TXT_SALAS);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.TXT_MOVIMIENTOS);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.pictureBox1);
            this.Name = "FrmPrincipal";
            this.Text = "FrmPrincipal";
            this.Load += new System.EventHandler(this.FrmPrincipal_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label TXT_MOVIMIENTOS;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label TXT_SALAS;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label txt_arbitro;
        private System.Windows.Forms.Label lbl_partida;
        private System.Windows.Forms.Label label5;
    }
}