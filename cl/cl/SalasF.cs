﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CLEntidad;
using CLLogica;

namespace cl
{
    public partial class SalasF : Form
    {

        SalasL _salasManejador = new SalasL();
        Salas_E _salasEntidad = new Salas_E();
        HotelL _hotelesManejador = new HotelL();
        VSalasL _VSalasl = new VSalasL();
        VSalasE _vSalasE = new VSalasE();
        public SalasF()
        {
            InitializeComponent();
            _hotelesManejador = new HotelL();
            _salasEntidad = new Salas_E();
            _salasManejador = new SalasL();
        }

        private void SalasF_Load(object sender, EventArgs e)
        {
            BuscarSalas("");
        }
        private void limpiarCuadros()
        {
            lbl_id.Text = "0";
            txt_medios.Text = "";
            txt_capacidad.Text = "";
            cmb_hotel.Text = "";
        }
        private void BuscarSalas(string filtro)
        {
            DgvSalas.DataSource = _VSalasl.GetSalas(filtro);//pasar la lista 
        }
        private void ModificarSalas()//funcion para  modificar  la tabla 
        {

            DataSet dAL;
            dAL = _hotelesManejador.GetidHotel(cmb_hotel.Text);
            lbl_id.Text = DgvSalas.CurrentRow.Cells["id_sala"].Value.ToString();
            txt_capacidad.Text = DgvSalas.CurrentRow.Cells["capacidad"].Value.ToString();
            txt_medios.Text = DgvSalas.CurrentRow.Cells["medios"].Value.ToString();
            _salasEntidad.Id_hotel = Convert.ToInt32(DgvSalas.CurrentRow.Cells["id_hotel"].Value.ToString());
            cmb_hotel.Text = DgvSalas.CurrentRow.Cells["nombre"].Value.ToString();
        }
        private void guardarSala()
        {
            _salasManejador.guardar(_salasEntidad);
        }
        private void EliminarSalas()
        {
            var id = DgvSalas.CurrentRow.Cells["Id_sala"].Value;//DEPENDE DEL DQATA
            _salasManejador.eliminar(Convert.ToInt32(id));//EL METODO LO NECESITA INT
        }
        private void CargarSalas()//crear global el objeto para usarlo en otro lugar 
        {
            DataSet dAL;
            dAL = _hotelesManejador.GetidHotel(cmb_hotel.Text);
            _salasEntidad.Id_sala = Convert.ToInt32(lbl_id.Text);
            _salasEntidad.Capacidad = Convert.ToInt32(txt_capacidad.Text);
            _salasEntidad.Medios = txt_medios.Text;
            _salasEntidad.Id_hotel = Convert.ToInt32(dAL.Tables[0].Rows[0]["id_hotel"].ToString());
            _vSalasE.Nombre = cmb_hotel.Text;

        }
        private void TraerHoteles(string filtro)
        {
            cmb_hotel.DataSource = _hotelesManejador.GetHoteles(filtro);
           cmb_hotel.DisplayMember = "nombre";
        }

        private void txt_buscar_TextChanged(object sender, EventArgs e)
        {
            BuscarSalas(txt_buscar.Text);
        }

        private void DgvSalas_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                ModificarSalas();
                BuscarSalas("");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void cmb_hotel_Click(object sender, EventArgs e)
        {
            TraerHoteles("");
        }

        private void btn__agregar_Click(object sender, EventArgs e)
        {
            txt_capacidad.Focus();
            limpiarCuadros();
        }

        private void btn_guardar_Click(object sender, EventArgs e)
        {
            CargarSalas();//primero se carga el usuario 


            try
            {

                guardarSala();//se lo pasamos a guardar usuario 
                limpiarCuadros();
                BuscarSalas(""); // mandar llamar al data grip con los nuevos datos
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
        }

        private void btn_cancelar_Click(object sender, EventArgs e)
        {
            limpiarCuadros();
        }

        private void btn_eliminar_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Estas seguro que deseas elimiar este registro", "Eliminar Resgistro", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                try
                {
                    EliminarSalas();
                    BuscarSalas("");
                }
                catch (Exception ex)
                {

                    MessageBox.Show(ex.Message);
                }
            }
        }
    }
}
