﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CLEntidad;
using CLLogica;

namespace cl
{
    public partial class PaisF : Form
    {
        private PaisL _paisL = new PaisL();
        private PaisE _paisE = new PaisE();
        public PaisF()
        {
            InitializeComponent();
            _paisE = new PaisE();
            _paisL = new PaisL();
        }

        private void PaisF_Load(object sender, EventArgs e)
        {
            BuscarPaises("");
        }

        private void BuscarPaises(string filtro)
        {
           DGV_PAISES.DataSource = _paisL.GetPaises(filtro);//pasar la lista 
        }
        private void ModificarPaises()//funcion para  modificar  la tabla 
        {
            LBL_IDP.Text = DGV_PAISES.CurrentRow.Cells["idpais"].Value.ToString();
            TXT_NOMBREP.Text = DGV_PAISES.CurrentRow.Cells["nombre"].Value.ToString();
            TXT_NUMP.Text = DGV_PAISES.CurrentRow.Cells["cantclub"].Value.ToString();
           CMB_REPRESENTAP.Text = DGV_PAISES.CurrentRow.Cells["id_paisr"].Value.ToString();
        }
   
        private void limpiarCuadros()
        {
            LBL_IDP.Text = "0";
            TXT_NUMP.Text = "";
            TXT_NOMBREP.Text = "";
            CMB_REPRESENTAP.Text = "";
        }
        private void EliminarPaises()//necesitamos el id int para que elimine el dato
        {
            var id = DGV_PAISES.CurrentRow.Cells["idpais"].Value;//DEPENDE DEL DQATA
            _paisL.eliminar(Convert.ToInt32(id));//EL METODO LO NECESITA INT
        }
        private void guardarPaises()
        {
            _paisL.guardar(_paisE);//sacar el usuario para mandarlo llamar en otro lugar
        }

        private void CargarPais()//crear global el objeto para usarlo en otro lugar 
        {

           _paisE.Idpais = Convert.ToInt32(LBL_IDP.Text);
            _paisE.Nombre = TXT_NOMBREP.Text;
            _paisE.Cantclub = Convert.ToInt32(TXT_NUMP.Text);
           _paisE.Id_paisr = CMB_REPRESENTAP.Text;
        }
        private void TraerPaises(string filtro)
        {
            CMB_REPRESENTAP.DataSource = _paisL.GetPaises(filtro);
            CMB_REPRESENTAP.DisplayMember = "Nombre";
        }

        private void TXT_BUSCRAP_TextChanged(object sender, EventArgs e)
        {
            BuscarPaises(TXT_BUSCRAP.Text);
        }

        private void BTN_AGREGARP_Click(object sender, EventArgs e)
        {
            TXT_NOMBREP.Focus();
            limpiarCuadros();
        }

        private void BTN_GUARDARP_Click(object sender, EventArgs e)
        {
            CargarPais();//primero se carga el usuario 


            try
            {

                guardarPaises();//se lo pasamos a guardar usuario 
                limpiarCuadros();
                BuscarPaises(""); // mandar llamar al data grip con los nuevos datos
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }



        }

        private void BTN_ELIMINARP_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Estas seguro que deseas elimiar este registro", "Eliminar Resgistro", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                try
                {
                    EliminarPaises();
                    BuscarPaises("");
                }
                catch (Exception ex)
                {

                    MessageBox.Show(ex.Message);
                }
            }
        }

        private void BTN_CANCELARP_Click(object sender, EventArgs e)
        {
            limpiarCuadros();
        }

        private void DGV_PAISES_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                ModificarPaises();
                BuscarPaises("");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void CMB_REPRESENTAP_Click(object sender, EventArgs e)
        {
            TraerPaises("");
        }
    }
}
