﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CLEntidad;
using CLLogica;

namespace cl
{
    public partial class JugadaP : Form
    {
        JugadaL _jugadaL = new JugadaL();
        JuagadaE _juagadaE = new JuagadaE();

        ParticipantesL _participantesL = new ParticipantesL();
        PartidaL _partidal = new PartidaL();
        

        public JugadaP()
        {
            InitializeComponent();
        }


        private void JugadaP_Load(object sender, EventArgs e)
        {
            Buscarjugadas("");
        }
        private void limpiarCuadros()
        {
            ldl_id.Text = "0";
            cmb_jugador.Text = "";
            cmb_partida.Text = "";
            txt_color.Text = "";
        }
        private void Traerjugador(string filtro)
        {
            cmb_jugador.DataSource = _participantesL.Getjugadores(filtro);
            cmb_jugador.DisplayMember = "nombre";
        }

        private void Traerpartida(string filtro)
        {
            cmb_partida.DataSource = _partidal.GetVpart(filtro);
            cmb_partida.DisplayMember = "PARTIDA";
        }
        private void Buscarjugadas(string filtro)
        {
            Dgv_jugada.DataSource = _jugadaL.GetvJugada(filtro);//pasar la lista 
        }
        private void ModificaJugada()//funcion para  modificar  la tabla 
        {
            ldl_id.Text = Dgv_jugada.CurrentRow.Cells["id_jugada"].Value.ToString();
            cmb_jugador.Text = Dgv_jugada.CurrentRow.Cells["id_jugador"].Value.ToString();
            txt_color.Text = Dgv_jugada.CurrentRow.Cells["color"].Value.ToString();
            cmb_partida.Text = Dgv_jugada.CurrentRow.Cells["id_partida"].Value.ToString();
        }
        private void Eliminarjugada()//necesitamos el id int para que elimine el dato
        {
            var id = Dgv_jugada.CurrentRow.Cells["Id_jugada"].Value;//DEPENDE DEL DQATA
            _jugadaL.eliminar(Convert.ToInt32(id));//EL METODO LO NECESITA INT
        }
        private void guardarjugada()
        {
            _jugadaL.guardar(_juagadaE);//sacar el usuario para mandarlo llamar en otro lugar
        }
        private void Cargarjugada()//crear global el objeto para usarlo en otro lugar 
        {
            DataSet dAL;
            dAL = _participantesL.Getidpart(cmb_jugador.Text);



            _juagadaE.Id_juagada = Convert.ToInt32(ldl_id.Text);
            _juagadaE.Id_jugador = Convert.ToInt32(dAL.Tables[0].Rows[0]["id_participante"].ToString());
            _juagadaE.Color = txt_color.Text;
           _juagadaE.Id_partida = Convert.ToInt32(cmb_partida.Text);
        }

        private void txt_buscarJ_TextChanged(object sender, EventArgs e)
        {
           Buscarjugadas(txt_buscarJ.Text);
        }

        private void btn_agregar_Click(object sender, EventArgs e)
        {
            limpiarCuadros();
            cmb_partida.Focus();
        }

        private void btn_guardar_Click(object sender, EventArgs e)
        {
            Cargarjugada();//primero se carga el usuario 


            try
            {

                guardarjugada();//se lo pasamos a guardar usuario 
                limpiarCuadros();
                Buscarjugadas(""); // mandar llamar al data grip con los nuevos datos
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
        }

        private void btn_eliminar_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Estas seguro que deseas elimiar este registro", "Eliminar Resgistro", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                try
                {
                    Eliminarjugada();
                    Buscarjugadas("");
                }
                catch (Exception ex)
                {

                    MessageBox.Show(ex.Message);
                }
            }
        }

        private void btn_cancelar_Click(object sender, EventArgs e)
        {
            limpiarCuadros();
        }

        private void Dgv_jugada_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void Dgv_jugada_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                ModificaJugada();
                Buscarjugadas("");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void cmb_jugador_Click(object sender, EventArgs e)
        {
            Traerjugador("");
        }

        private void cmb_partida_Click(object sender, EventArgs e)
        {
            Traerpartida("");
        }
    }
}
