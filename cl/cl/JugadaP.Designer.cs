﻿namespace cl
{
    partial class JugadaP
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(JugadaP));
            this.txt_buscarJ = new System.Windows.Forms.TextBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.btn_agregar = new System.Windows.Forms.Button();
            this.btn_guardar = new System.Windows.Forms.Button();
            this.btn_eliminar = new System.Windows.Forms.Button();
            this.btn_cancelar = new System.Windows.Forms.Button();
            this.Dgv_jugada = new System.Windows.Forms.DataGridView();
            this.txt_color = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.cmb_jugador = new System.Windows.Forms.ComboBox();
            this.cmb_partida = new System.Windows.Forms.ComboBox();
            this.ldl_id = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Dgv_jugada)).BeginInit();
            this.SuspendLayout();
            // 
            // txt_buscarJ
            // 
            this.txt_buscarJ.Location = new System.Drawing.Point(48, 23);
            this.txt_buscarJ.Name = "txt_buscarJ";
            this.txt_buscarJ.Size = new System.Drawing.Size(313, 20);
            this.txt_buscarJ.TabIndex = 0;
            this.txt_buscarJ.TextChanged += new System.EventHandler(this.txt_buscarJ_TextChanged);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(378, 23);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(26, 22);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 63;
            this.pictureBox1.TabStop = false;
            // 
            // btn_agregar
            // 
            this.btn_agregar.Location = new System.Drawing.Point(637, 23);
            this.btn_agregar.Name = "btn_agregar";
            this.btn_agregar.Size = new System.Drawing.Size(75, 23);
            this.btn_agregar.TabIndex = 64;
            this.btn_agregar.Text = "AGREGAR";
            this.btn_agregar.UseVisualStyleBackColor = true;
            this.btn_agregar.Click += new System.EventHandler(this.btn_agregar_Click);
            // 
            // btn_guardar
            // 
            this.btn_guardar.Location = new System.Drawing.Point(637, 52);
            this.btn_guardar.Name = "btn_guardar";
            this.btn_guardar.Size = new System.Drawing.Size(75, 23);
            this.btn_guardar.TabIndex = 65;
            this.btn_guardar.Text = "GUARDAR";
            this.btn_guardar.UseVisualStyleBackColor = true;
            this.btn_guardar.Click += new System.EventHandler(this.btn_guardar_Click);
            // 
            // btn_eliminar
            // 
            this.btn_eliminar.Location = new System.Drawing.Point(637, 81);
            this.btn_eliminar.Name = "btn_eliminar";
            this.btn_eliminar.Size = new System.Drawing.Size(75, 23);
            this.btn_eliminar.TabIndex = 66;
            this.btn_eliminar.Text = "ELIMINAR";
            this.btn_eliminar.UseVisualStyleBackColor = true;
            this.btn_eliminar.Click += new System.EventHandler(this.btn_eliminar_Click);
            // 
            // btn_cancelar
            // 
            this.btn_cancelar.Location = new System.Drawing.Point(637, 111);
            this.btn_cancelar.Name = "btn_cancelar";
            this.btn_cancelar.Size = new System.Drawing.Size(75, 23);
            this.btn_cancelar.TabIndex = 67;
            this.btn_cancelar.Text = "CANCELAR";
            this.btn_cancelar.UseVisualStyleBackColor = true;
            this.btn_cancelar.Click += new System.EventHandler(this.btn_cancelar_Click);
            // 
            // Dgv_jugada
            // 
            this.Dgv_jugada.BackgroundColor = System.Drawing.Color.SteelBlue;
            this.Dgv_jugada.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.Dgv_jugada.Location = new System.Drawing.Point(48, 244);
            this.Dgv_jugada.Name = "Dgv_jugada";
            this.Dgv_jugada.Size = new System.Drawing.Size(678, 150);
            this.Dgv_jugada.TabIndex = 68;
            this.Dgv_jugada.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.Dgv_jugada_CellContentClick);
            this.Dgv_jugada.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.Dgv_jugada_CellDoubleClick);
            // 
            // txt_color
            // 
            this.txt_color.Location = new System.Drawing.Point(188, 123);
            this.txt_color.Name = "txt_color";
            this.txt_color.Size = new System.Drawing.Size(180, 20);
            this.txt_color.TabIndex = 69;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(79, 91);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(59, 13);
            this.label1.TabIndex = 70;
            this.label1.Text = "JUGADOR";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(79, 130);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(44, 13);
            this.label2.TabIndex = 71;
            this.label2.Text = "COLOR";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(79, 166);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(57, 13);
            this.label3.TabIndex = 72;
            this.label3.Text = "PARTIDA ";
            // 
            // cmb_jugador
            // 
            this.cmb_jugador.FormattingEnabled = true;
            this.cmb_jugador.Location = new System.Drawing.Point(188, 82);
            this.cmb_jugador.Name = "cmb_jugador";
            this.cmb_jugador.Size = new System.Drawing.Size(180, 21);
            this.cmb_jugador.TabIndex = 73;
            this.cmb_jugador.Click += new System.EventHandler(this.cmb_jugador_Click);
            // 
            // cmb_partida
            // 
            this.cmb_partida.FormattingEnabled = true;
            this.cmb_partida.Location = new System.Drawing.Point(188, 166);
            this.cmb_partida.Name = "cmb_partida";
            this.cmb_partida.Size = new System.Drawing.Size(180, 21);
            this.cmb_partida.TabIndex = 74;
            this.cmb_partida.Click += new System.EventHandler(this.cmb_partida_Click);
            // 
            // ldl_id
            // 
            this.ldl_id.AutoSize = true;
            this.ldl_id.Location = new System.Drawing.Point(728, 26);
            this.ldl_id.Name = "ldl_id";
            this.ldl_id.Size = new System.Drawing.Size(13, 13);
            this.ldl_id.TabIndex = 75;
            this.ldl_id.Text = "0";
            // 
            // JugadaP
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.SteelBlue;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.ldl_id);
            this.Controls.Add(this.cmb_partida);
            this.Controls.Add(this.cmb_jugador);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txt_color);
            this.Controls.Add(this.Dgv_jugada);
            this.Controls.Add(this.btn_cancelar);
            this.Controls.Add(this.btn_eliminar);
            this.Controls.Add(this.btn_guardar);
            this.Controls.Add(this.btn_agregar);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.txt_buscarJ);
            this.Name = "JugadaP";
            this.Text = "JugadaP";
            this.Load += new System.EventHandler(this.JugadaP_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Dgv_jugada)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txt_buscarJ;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button btn_agregar;
        private System.Windows.Forms.Button btn_guardar;
        private System.Windows.Forms.Button btn_eliminar;
        private System.Windows.Forms.Button btn_cancelar;
        private System.Windows.Forms.DataGridView Dgv_jugada;
        private System.Windows.Forms.TextBox txt_color;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox cmb_jugador;
        private System.Windows.Forms.ComboBox cmb_partida;
        private System.Windows.Forms.Label ldl_id;
    }
}