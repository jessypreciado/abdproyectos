﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CLEntidad;
using CLLogica;

namespace cl
{
    public partial class MovimientosF : Form
    {
        Movimiento_L _movimientosManejador = new Movimiento_L();
        Movimientos_E _movimentosEntidad = new Movimientos_E();
        public MovimientosF()
        {
            InitializeComponent();
          
            _movimentosEntidad = new Movimientos_E();
            _movimientosManejador = new Movimiento_L();
        }

        private void MovimientosF_Load(object sender, EventArgs e)
        {
            BuscarMovimiento("");
        }
        private void BuscarMovimiento(string filtro)
        {
            DgvMovimientos.DataSource = _movimientosManejador.Getmovimientos(filtro);//pasar la lista 
        }
        private void limpiarCuadros()
        {
            lblId.Text = "0";
            TXT_NOMBRE.Text = "";
        }
        private void ModificarMovimiento()//funcion para  modificar  la tabla 
        {

        
            lblId.Text = DgvMovimientos.CurrentRow.Cells["id_movimiento"].Value.ToString();
            TXT_NOMBRE.Text = DgvMovimientos.CurrentRow.Cells["nombre"].Value.ToString();
        }
        private void EliminarMov()
        {
            var id = DgvMovimientos.CurrentRow.Cells["id_movimiento"].Value;//DEPENDE DEL DQATA
            _movimientosManejador.eliminar(Convert.ToInt32(id));//EL METODO LO NECESITA INT
        }
        private void guardarMovimiento()
        {
            _movimientosManejador.guardar(_movimentosEntidad);
        }
        private void CargarMovimiento()//crear global el objeto para usarlo en otro lugar 
        {

            _movimentosEntidad.Id_movimiento = Convert.ToInt32(lblId.Text);
            _movimentosEntidad.Nombre = TXT_NOMBRE.Text;
        }

        private void TXT_BUSCAR_TextChanged(object sender, EventArgs e)
        {
            BuscarMovimiento(TXT_BUSCAR.Text);
        }

        private void DgvMovimientos_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                ModificarMovimiento();
                BuscarMovimiento("");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btn_guardar_Click(object sender, EventArgs e)
        {
            CargarMovimiento();//primero se carga el usuario 


            try
            {

                guardarMovimiento();//se lo pasamos a guardar usuario 
                BuscarMovimiento(""); // mandar llamar al data grip con los nuevos datos
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
        }

        private void btn_eliminar_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Estas seguro que deseas elimiar este registro", "Eliminar Resgistro", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                try
                {
                    EliminarMov();
                    BuscarMovimiento("");
                }
                catch (Exception ex)
                {

                    MessageBox.Show(ex.Message);
                }
            }
        }

        private void btn_cancelar_Click(object sender, EventArgs e)
        {
            limpiarCuadros();
        }

        private void btn_agregar_Click(object sender, EventArgs e)
        {
            limpiarCuadros();
            TXT_NOMBRE.Focus();
        }
    }
}
