﻿namespace cl
{
    partial class HotelF
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(HotelF));
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.TXT_BUSCARH = new System.Windows.Forms.TextBox();
            this.TXT_NOMBREH = new System.Windows.Forms.TextBox();
            this.TXT_DIRECCIONH = new System.Windows.Forms.TextBox();
            this.TXT_TELEFONOH = new System.Windows.Forms.TextBox();
            this.DGV_HOTEL = new System.Windows.Forms.DataGridView();
            this.BTN_AGREGARH = new System.Windows.Forms.Button();
            this.BTN_GUARDARH = new System.Windows.Forms.Button();
            this.BTN_ELIMINARH = new System.Windows.Forms.Button();
            this.BTN_CANCELARH = new System.Windows.Forms.Button();
            this.LBL_ID = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.DGV_HOTEL)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Berlin Sans FB", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(63, 178);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(64, 13);
            this.label3.TabIndex = 28;
            this.label3.Text = "TELEFONO";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Berlin Sans FB", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(63, 140);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(66, 13);
            this.label2.TabIndex = 27;
            this.label2.Text = "DIRECCION";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Berlin Sans FB", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(60, 111);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(54, 13);
            this.label1.TabIndex = 26;
            this.label1.Text = "NOMBRE";
            // 
            // TXT_BUSCARH
            // 
            this.TXT_BUSCARH.Location = new System.Drawing.Point(66, 36);
            this.TXT_BUSCARH.Name = "TXT_BUSCARH";
            this.TXT_BUSCARH.Size = new System.Drawing.Size(276, 20);
            this.TXT_BUSCARH.TabIndex = 29;
            this.TXT_BUSCARH.TextChanged += new System.EventHandler(this.TXT_BUSCARH_TextChanged);
            // 
            // TXT_NOMBREH
            // 
            this.TXT_NOMBREH.Location = new System.Drawing.Point(168, 103);
            this.TXT_NOMBREH.Name = "TXT_NOMBREH";
            this.TXT_NOMBREH.Size = new System.Drawing.Size(152, 20);
            this.TXT_NOMBREH.TabIndex = 30;
            // 
            // TXT_DIRECCIONH
            // 
            this.TXT_DIRECCIONH.Location = new System.Drawing.Point(168, 140);
            this.TXT_DIRECCIONH.Name = "TXT_DIRECCIONH";
            this.TXT_DIRECCIONH.Size = new System.Drawing.Size(152, 20);
            this.TXT_DIRECCIONH.TabIndex = 31;
            // 
            // TXT_TELEFONOH
            // 
            this.TXT_TELEFONOH.Location = new System.Drawing.Point(168, 178);
            this.TXT_TELEFONOH.Name = "TXT_TELEFONOH";
            this.TXT_TELEFONOH.Size = new System.Drawing.Size(152, 20);
            this.TXT_TELEFONOH.TabIndex = 32;
            // 
            // DGV_HOTEL
            // 
            this.DGV_HOTEL.BackgroundColor = System.Drawing.Color.SteelBlue;
            this.DGV_HOTEL.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DGV_HOTEL.Location = new System.Drawing.Point(21, 241);
            this.DGV_HOTEL.Name = "DGV_HOTEL";
            this.DGV_HOTEL.Size = new System.Drawing.Size(549, 150);
            this.DGV_HOTEL.TabIndex = 33;
            this.DGV_HOTEL.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DGV_HOTEL_CellDoubleClick);
            // 
            // BTN_AGREGARH
            // 
            this.BTN_AGREGARH.Font = new System.Drawing.Font("Berlin Sans FB", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BTN_AGREGARH.Location = new System.Drawing.Point(495, 103);
            this.BTN_AGREGARH.Name = "BTN_AGREGARH";
            this.BTN_AGREGARH.Size = new System.Drawing.Size(75, 23);
            this.BTN_AGREGARH.TabIndex = 34;
            this.BTN_AGREGARH.Text = "AGREGAR";
            this.BTN_AGREGARH.UseVisualStyleBackColor = true;
            this.BTN_AGREGARH.Click += new System.EventHandler(this.BTN_AGREGARH_Click);
            // 
            // BTN_GUARDARH
            // 
            this.BTN_GUARDARH.Font = new System.Drawing.Font("Berlin Sans FB", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BTN_GUARDARH.Location = new System.Drawing.Point(495, 135);
            this.BTN_GUARDARH.Name = "BTN_GUARDARH";
            this.BTN_GUARDARH.Size = new System.Drawing.Size(75, 23);
            this.BTN_GUARDARH.TabIndex = 35;
            this.BTN_GUARDARH.Text = "GUARDAR";
            this.BTN_GUARDARH.UseVisualStyleBackColor = true;
            this.BTN_GUARDARH.Click += new System.EventHandler(this.BTN_GUARDARH_Click);
            // 
            // BTN_ELIMINARH
            // 
            this.BTN_ELIMINARH.Font = new System.Drawing.Font("Berlin Sans FB", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BTN_ELIMINARH.Location = new System.Drawing.Point(495, 164);
            this.BTN_ELIMINARH.Name = "BTN_ELIMINARH";
            this.BTN_ELIMINARH.Size = new System.Drawing.Size(75, 23);
            this.BTN_ELIMINARH.TabIndex = 36;
            this.BTN_ELIMINARH.Text = "ELIMINAR";
            this.BTN_ELIMINARH.UseVisualStyleBackColor = true;
            this.BTN_ELIMINARH.Click += new System.EventHandler(this.BTN_ELIMINARH_Click);
            // 
            // BTN_CANCELARH
            // 
            this.BTN_CANCELARH.Font = new System.Drawing.Font("Berlin Sans FB", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BTN_CANCELARH.Location = new System.Drawing.Point(495, 193);
            this.BTN_CANCELARH.Name = "BTN_CANCELARH";
            this.BTN_CANCELARH.Size = new System.Drawing.Size(75, 23);
            this.BTN_CANCELARH.TabIndex = 37;
            this.BTN_CANCELARH.Text = "CANCELAR";
            this.BTN_CANCELARH.UseVisualStyleBackColor = true;
            this.BTN_CANCELARH.Click += new System.EventHandler(this.BTN_CANCELARH_Click);
            // 
            // LBL_ID
            // 
            this.LBL_ID.AutoSize = true;
            this.LBL_ID.Font = new System.Drawing.Font("Berlin Sans FB", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LBL_ID.Location = new System.Drawing.Point(479, 43);
            this.LBL_ID.Name = "LBL_ID";
            this.LBL_ID.Size = new System.Drawing.Size(14, 13);
            this.LBL_ID.TabIndex = 38;
            this.LBL_ID.Text = "0";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(384, 43);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(26, 22);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 39;
            this.pictureBox1.TabStop = false;
            // 
            // HotelF
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.SteelBlue;
            this.ClientSize = new System.Drawing.Size(598, 412);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.LBL_ID);
            this.Controls.Add(this.BTN_CANCELARH);
            this.Controls.Add(this.BTN_ELIMINARH);
            this.Controls.Add(this.BTN_GUARDARH);
            this.Controls.Add(this.BTN_AGREGARH);
            this.Controls.Add(this.DGV_HOTEL);
            this.Controls.Add(this.TXT_TELEFONOH);
            this.Controls.Add(this.TXT_DIRECCIONH);
            this.Controls.Add(this.TXT_NOMBREH);
            this.Controls.Add(this.TXT_BUSCARH);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "HotelF";
            this.Text = "HotelF";
            this.Load += new System.EventHandler(this.HotelF_Load);
            ((System.ComponentModel.ISupportInitialize)(this.DGV_HOTEL)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox TXT_BUSCARH;
        private System.Windows.Forms.TextBox TXT_NOMBREH;
        private System.Windows.Forms.TextBox TXT_DIRECCIONH;
        private System.Windows.Forms.TextBox TXT_TELEFONOH;
        private System.Windows.Forms.DataGridView DGV_HOTEL;
        private System.Windows.Forms.Button BTN_AGREGARH;
        private System.Windows.Forms.Button BTN_GUARDARH;
        private System.Windows.Forms.Button BTN_ELIMINARH;
        private System.Windows.Forms.Button BTN_CANCELARH;
        private System.Windows.Forms.Label LBL_ID;
        private System.Windows.Forms.PictureBox pictureBox1;
    }
}