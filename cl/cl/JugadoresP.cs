﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CLEntidad;
using CLLogica;

namespace cl
{
    public partial class JugadoresP : Form
    {
       
        PaisL _paisesl = new PaisL();

        ParticipantesE _participantesE = new ParticipantesE();
        ParticipantesL _participantesL = new ParticipantesL();
        public JugadoresP()
        {
            InitializeComponent();
            _participantesE = new ParticipantesE();
            _participantesL = new ParticipantesL();
            _paisesl = new PaisL();
            
        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void JugadoresP_Load(object sender, EventArgs e)
        {
            BuscarJugadores("");
            
        }
        private void limpiarCuadros()
        {
            LBL_ID.Text = "0";
            TXT_CODIGO.Text = "";
            TXT_DIRECCION.Text = "";
            TXT_NIVELJUEGO.Text = "";
            TXT_NOMBRE.Text = "";
            TXT_TELEFONO.Text = "";
            CMB_PAIS.Text = "";

        }
        private void BuscarJugadores(string filtro)
        {
            DGV_JUGADORES.DataSource = _participantesL.Getjugadores(filtro);//pasar la lista 
        }
        private void ModificarJuugadores()//funcion para  modificar  la tabla 
        {
            LBL_ID.Text = DGV_JUGADORES.CurrentRow.Cells["id"].Value.ToString();
            TXT_CODIGO.Text = DGV_JUGADORES.CurrentRow.Cells["id_participante"].Value.ToString();
            
            TXT_NOMBRE.Text = DGV_JUGADORES.CurrentRow.Cells["nombre"].Value.ToString();
            TXT_DIRECCION.Text = DGV_JUGADORES.CurrentRow.Cells["direccion"].Value.ToString();
            TXT_TELEFONO.Text = DGV_JUGADORES.CurrentRow.Cells["telefono"].Value.ToString();
            CMB_PAIS.Text = DGV_JUGADORES.CurrentRow.Cells["pais"].Value.ToString();
            TXT_NIVELJUEGO.Text = DGV_JUGADORES.CurrentRow.Cells["niveljuego"].Value.ToString();
            

        }

        private void EliminarJugadores()//necesitamos el id int para que elimine el dato
        {
            var id = DGV_JUGADORES.CurrentRow.Cells["id_participante"].Value;//DEPENDE DEL DQATA
            _participantesL.eliminar(id.ToString());//EL METODO LO NECESITA I
        }

        private void guardarJugadores()
        {
            _participantesL.guardar(_participantesE);
        }
        private void CargarJugadores()//crear global el objeto para usarlo en otro lugar 
        {
            
 
            _participantesE.Id = Convert.ToInt32(LBL_ID.Text);
            _participantesE.Id_participante = TXT_CODIGO.Text;
            _participantesE.Nombre = TXT_NOMBRE.Text;
            _participantesE.Direccion = TXT_DIRECCION.Text;
            _participantesE.Telefono = TXT_TELEFONO.Text;
            _participantesE.Tipo = 1;
            DataSet dAL;
            dAL = _paisesl.GetidPaises(CMB_PAIS.Text);
            _participantesE.Id_pais = Convert.ToInt32(dAL.Tables[0].Rows[0]["id_pais"].ToString());
            _participantesE.Niveljuego= Convert.ToInt32(TXT_NIVELJUEGO.Text);

        }
        private void Traerpais(string filtro)
        {
            CMB_PAIS.DataSource = _paisesl.GetPaises(filtro);
            CMB_PAIS.DisplayMember = "nombre";
        }

        private void CMB_PAIS_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void CMB_PAIS_Click(object sender, EventArgs e)
        {
            Traerpais("");
        }

        private void textBox5_TextChanged(object sender, EventArgs e)
        {
            BuscarJugadores(txt_buscar.Text);
        }

        private void BTN_AGREGAR_Click(object sender, EventArgs e)
        {
            TXT_CODIGO.Enabled = true;
            limpiarCuadros();
            TXT_CODIGO.Focus();
        }

        private void BTN_GUARDAR_Click(object sender, EventArgs e)
        {
            
            CargarJugadores();
            try
            {
                
                guardarJugadores();//se lo pasamos a guardar usuario 
                limpiarCuadros();
                TXT_CODIGO.Enabled = true;
                BuscarJugadores(""); // mandar llamar al data grip con los nuevos datos
                
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
        }

        private void BTN_ELIMINAR_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Estas seguro que deseas elimiar este registro", "Eliminar Resgistro", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                try
                {
                    EliminarJugadores();
                    BuscarJugadores("");
                }
                catch (Exception ex)
                {

                    MessageBox.Show(ex.Message);
                }
            }
        }

        private void BTN_CANCELAR_Click(object sender, EventArgs e)
        {
            limpiarCuadros();
        }

        private void DGV_JUGADORES_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            TXT_CODIGO.Enabled = false;
            try
            {
                ModificarJuugadores();
                BuscarJugadores("");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }

    
}