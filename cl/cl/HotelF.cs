﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CLEntidad;
using CLLogica;
namespace cl
{
    public partial class HotelF : Form
    {

        private HotelL _hotelL = new HotelL();
        private Hotel_E _hotel_E= new Hotel_E();
        public HotelF()
        {
            InitializeComponent();
            _hotel_E = new Hotel_E();
            _hotelL = new HotelL();
        }

        private void HotelF_Load(object sender, EventArgs e)
        {
            BuscarHotel("");
        }
       
        private void limpiarCuadros()
        {
            LBL_ID.Text = "0";
            TXT_DIRECCIONH.Text = "";
            TXT_NOMBREH.Text = "";
            TXT_TELEFONOH.Text = "";
        }
        private void BuscarHotel(string filtro)
        {
            DGV_HOTEL.DataSource = _hotelL.GetHoteles(filtro);//pasar la lista 
        }
        private void ModificarHoteles()//funcion para  modificar  la tabla 
        {
            LBL_ID.Text = DGV_HOTEL.CurrentRow.Cells["id_hotel"].Value.ToString();
            TXT_NOMBREH.Text = DGV_HOTEL.CurrentRow.Cells["nombre"].Value.ToString();
            TXT_DIRECCIONH.Text = DGV_HOTEL.CurrentRow.Cells["direccion"].Value.ToString();
            TXT_TELEFONOH.Text = DGV_HOTEL.CurrentRow.Cells["telefono"].Value.ToString();
        }
        private void EliminarHotel()//necesitamos el id int para que elimine el dato
        {
            var id = DGV_HOTEL.CurrentRow.Cells["id_hotel"].Value;//DEPENDE DEL DQATA
            _hotelL.eliminar(Convert.ToInt32(id));//EL METODO LO NECESITA INT
        }
        private void guardarHotel()
        {
            _hotelL.guardar(_hotel_E);//sacar el usuario para mandarlo llamar en otro lugar
        }

        private void CargarHotel()//crear global el objeto para usarlo en otro lugar 
        {

            _hotel_E.Id_hotel = Convert.ToInt32(LBL_ID.Text);
            _hotel_E.Nombre = TXT_NOMBREH.Text;
           _hotel_E.Direccion = TXT_DIRECCIONH.Text;
            _hotel_E.Telefono = TXT_TELEFONOH.Text;


        }

        private void TXT_BUSCARH_TextChanged(object sender, EventArgs e)
        {
            BuscarHotel(TXT_BUSCARH.Text);
        }

        private void BTN_AGREGARH_Click(object sender, EventArgs e)
        {
            limpiarCuadros();
            TXT_NOMBREH.Focus();
        }

        private void BTN_GUARDARH_Click(object sender, EventArgs e)
        {
            CargarHotel();//primero se carga el usuario 


            try
            {

                guardarHotel();//se lo pasamos a guardar usuario 

                BuscarHotel(""); // mandar llamar al data grip con los nuevos datos
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
        }

        private void BTN_ELIMINARH_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Estas seguro que deseas elimiar este registro", "Eliminar Resgistro", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                try
                {
                    EliminarHotel();
                    BuscarHotel("");
                }
                catch (Exception ex)
                {

                    MessageBox.Show(ex.Message);
                }
            }
        }

        private void BTN_CANCELARH_Click(object sender, EventArgs e)
        {
            limpiarCuadros();
        }

        private void DGV_HOTEL_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                ModificarHoteles();
                BuscarHotel("");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
