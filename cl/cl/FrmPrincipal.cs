﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace cl
{
    public partial class FrmPrincipal : Form
    {
        HotelF _hotelf = new HotelF();
        MovimientosF _movimientoF = new MovimientosF();
        PaisF _paisF = new PaisF();
        SalasF _SalasF = new SalasF();
        ReservacionesP _reservacionesP = new ReservacionesP();
        JugadoresP _jugadoresP = new JugadoresP();
        ArbitrosP _ArbitrosP = new ArbitrosP();
        PartidaP _partidaP = new PartidaP();
        JugadaP _jugadaP = new JugadaP();
        public FrmPrincipal()
        {
            InitializeComponent();
        }

        private void toolStripLabel1_Click(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {
            _hotelf.ShowDialog();
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }

        private void TXT_MOVIMIENTOS_Click(object sender, EventArgs e)
        {
            _movimientoF.ShowDialog();
        }

        private void label2_Click(object sender, EventArgs e)
        {
            _paisF.ShowDialog();
        }

        private void TXT_SALAS_Click(object sender, EventArgs e)
        {
            _SalasF.ShowDialog();
        }

        private void label3_Click(object sender, EventArgs e)
        {
            _reservacionesP.ShowDialog();
        }

        private void FrmPrincipal_Load(object sender, EventArgs e)
        {

        }

        private void label4_Click(object sender, EventArgs e)
        {
            _jugadoresP.ShowDialog();
        }

        private void txt_arbitro_Click(object sender, EventArgs e)
        {
            _ArbitrosP.ShowDialog();
        }

        private void lbl_partida_Click(object sender, EventArgs e)
        {
            _partidaP.ShowDialog();
        }

        private void label5_Click(object sender, EventArgs e)
        {
            _jugadaP.ShowDialog();
        }
    }
}
