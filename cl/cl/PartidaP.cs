﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CLEntidad;
using CLLogica;


namespace cl
{
    public partial class PartidaP : Form
    {
        PartidasE _PARTIDAE = new PartidasE();
        PartidaL _PARTIDAL = new PartidaL();

        VPartida _vPartida = new VPartida();
        SalasL _salasL = new SalasL();
        Movimiento_L _movimiento_L = new Movimiento_L();
        ParticipantesL _participantesL = new ParticipantesL();
        VSalasL _vSalasL = new VSalasL();

     



        public PartidaP()
        {
            InitializeComponent();
            _PARTIDAE = new PartidasE();
            _PARTIDAL = new PartidaL();
            _vPartida = new VPartida();
            _salasL = new SalasL();
            _movimiento_L = new Movimiento_L();
            _participantesL = new ParticipantesL();
            _vSalasL = new VSalasL();
        }

        private void PartidaP_Load(object sender, EventArgs e)
        {
            BuscarPARTIDA("");
        }
        private void limpiarCuadros()
        {
            LBL_ID.Text = "0";
            TXT_ID_PART.Text = "";
            CMB_MOV.Text = "";
            CMB_ARBITRO.Text = "";
            CMB_SALA.Text = "";

        }
        private void BuscarPARTIDA(string filtro)
        {
            DGV_PARTIDA.DataSource = _PARTIDAL.GetVpart(filtro);//pasar la lista 
        }
        private void ModificarPartida()//funcion para  modificar  la tabla 
        {
            LBL_ID.Text = DGV_PARTIDA.CurrentRow.Cells["id"].Value.ToString();
         
            TXT_ID_PART.Text = DGV_PARTIDA.CurrentRow.Cells["PARTIDA"].Value.ToString();
            CMB_MOV.Text = DGV_PARTIDA.CurrentRow.Cells["MOVIMIENTO"].Value.ToString();
            CMB_ARBITRO.Text = DGV_PARTIDA.CurrentRow.Cells["ARBITRO"].Value.ToString();
            CMB_SALA.Text = DGV_PARTIDA.CurrentRow.Cells["ID_SALA"].Value.ToString();

        }

        private void EliminarPartida()//necesitamos el id int para que elimine el dato
        {
            var id = DGV_PARTIDA.CurrentRow.Cells["PARTIDA"].Value;//DEPENDE DEL DQATA
            _PARTIDAL.eliminar(Convert.ToInt32(id));//EL METODO LO NECESITA INT
        }
        private void guardaraPartida()
        {
            _PARTIDAL.guardar(_PARTIDAE);
        }
        private void CargarPartida()//crear global el objeto para usarlo en otro lugar 
        {
            DataSet dAL;
            dAL = _movimiento_L.Getidmov(CMB_MOV.Text);
            DataSet dlL;
            dlL = _participantesL.GetiParticipantes(CMB_ARBITRO.Text);

            _PARTIDAE.Id = Convert.ToInt32(LBL_ID.Text);
            _PARTIDAE.Id_partida = Convert.ToInt32(TXT_ID_PART.Text);
            _PARTIDAE.Id_movimiento = Convert.ToInt32(dAL.Tables[0].Rows[0]["id_movimiento"].ToString());

            _PARTIDAE.Id_arbitro = Convert.ToInt32(dlL.Tables[0].Rows[0]["id_participante"].ToString());
            _PARTIDAE.Id_sala = Convert.ToInt32(CMB_SALA.Text);
      }
        private void Traermovimiento(string filtro)
        {
            CMB_MOV.DataSource = _movimiento_L.Getmovimientos(filtro);
            CMB_MOV.DisplayMember = "nombre";
        }
        private void Traerarbitro(string filtro)
        {
            CMB_ARBITRO.DataSource = _participantesL.GetArbitro(filtro);
            CMB_ARBITRO.DisplayMember = "nombre";
        }
        private void Traersala(string filtro)
        {
            CMB_SALA.DataSource = _vSalasL.GetSalas("");
            CMB_SALA.DisplayMember = "id_sala";
        }

        private void CMB_MOV_Click(object sender, EventArgs e)
        {
            Traermovimiento("");
        }

        private void CMB_ARBITRO_Click(object sender, EventArgs e)
        {
            Traerarbitro("");

        }

        private void CMB_SALA_Click(object sender, EventArgs e)
        {
            Traersala("");

        }

        private void XT_BUSCARPR_TextChanged(object sender, EventArgs e)
        {
            BuscarPARTIDA(TXT_ID_PART.Text);
        }

        private void BTN_AGREGAR_Click(object sender, EventArgs e)
        {
            limpiarCuadros();
            TXT_ID_PART.Focus();
        }

        private void BTN_GUARDAR_Click(object sender, EventArgs e)
        {
            CargarPartida();
            try
            {

                guardaraPartida();//se lo pasamos a guardar usuario 
                limpiarCuadros();
                BuscarPARTIDA(""); // mandar llamar al data grip con los nuevos datos
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
        }

        private void BTN_ELIMINAR_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Estas seguro que deseas elimiar este registro", "Eliminar Resgistro", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                try
                {
                    EliminarPartida();
                    BuscarPARTIDA("");
                }
                catch (Exception ex)
                {

                    MessageBox.Show(ex.Message);
                }
            }
        }

        private void BTN_CANCELAR_Click(object sender, EventArgs e)
        {
            limpiarCuadros();
        }

        private void DGV_PARTIDA_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                ModificarPartida();
                BuscarPARTIDA("");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void CMB_ARBITRO_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}
