﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CLEntidad;
using CLLogica;

namespace cl
{
    public partial class ArbitrosP : Form
    {
        PaisL _paisesl = new PaisL();

        ParticipantesE _participantesE = new ParticipantesE();
        ParticipantesL _participantesL = new ParticipantesL();
        public ArbitrosP()
        {
            InitializeComponent();
            _participantesE = new ParticipantesE();
            _participantesL = new ParticipantesL();
            _paisesl = new PaisL();

        }

        private void ArbitrosP_Load(object sender, EventArgs e)
        {
            BuscarArbitros("");
        }
        private void BuscarArbitros(string filtro)
        {
           DGV_ARBITROS.DataSource = _participantesL.GetArbitro(filtro);//pasar la lista 
        }
        private void limpiarCuadros()
        {
            lbl_id.Text = "0";
            TXT_CODIGO.Text = "";
            TXT_DIRECCION.Text = "";
            TXT_NOMBRE.Text = "";
            TXT_TELEFONO.Text = "";
            CMB_PAIS.Text = "";
        }
        private void Modificararbitros()//funcion para  modificar  la tabla 
        {

            lbl_id.Text = DGV_ARBITROS.CurrentRow.Cells["id"].Value.ToString();
            TXT_CODIGO.Text = DGV_ARBITROS.CurrentRow.Cells["id_participante"].Value.ToString();
            TXT_NOMBRE.Text = DGV_ARBITROS.CurrentRow.Cells["nombre"].Value.ToString();
            TXT_DIRECCION.Text = DGV_ARBITROS.CurrentRow.Cells["direccion"].Value.ToString();
            TXT_TELEFONO.Text = DGV_ARBITROS.CurrentRow.Cells["telefono"].Value.ToString();
            CMB_PAIS.Text = DGV_ARBITROS.CurrentRow.Cells["pais"].Value.ToString();
            
        }
        private void guardararbitro()
        {
           _participantesL.guardarArbitro(_participantesE);
        }
        private void Eliminararbitro()
        {
            var id = DGV_ARBITROS.CurrentRow.Cells["id_participante"].Value;//DEPENDE DEL DQATA
            _participantesL.eliminarArbitro(id.ToString());//EL METODO LO NECESITA INT
        }
        private void Cargararbitro()//crear global el objeto para usarlo en otro lugar 
        {
            _participantesE.Id = Convert.ToInt32(lbl_id.Text);
            _participantesE.Id_participante = TXT_CODIGO.Text;
            _participantesE.Nombre = TXT_NOMBRE.Text;
            _participantesE.Direccion = TXT_DIRECCION.Text;
            _participantesE.Telefono = TXT_TELEFONO.Text;
            _participantesE.Tipo = 2;
            DataSet dAL;
            dAL = _paisesl.GetidPaises(CMB_PAIS.Text);
            _participantesE.Id_pais = Convert.ToInt32(dAL.Tables[0].Rows[0]["id_pais"].ToString());
            
        }
        private void Traerpais(string filtro)
        {
            CMB_PAIS.DataSource = _paisesl.GetPaises(filtro);
            CMB_PAIS.DisplayMember = "nombre";
        }

        private void CMB_PAIS_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void CMB_PAIS_Click(object sender, EventArgs e)
        {
            Traerpais("");
        }

        private void TXT_BUSCAR_TextChanged(object sender, EventArgs e)
        {
            BuscarArbitros(TXT_BUSCAR.Text);
        }

        private void BTN_AGREGAR_Click(object sender, EventArgs e)
        {
            TXT_CODIGO.Enabled = true;
            limpiarCuadros();
            TXT_BUSCAR.Focus();
        }

        private void BTN_GUADAR_Click(object sender, EventArgs e)
        {
            Cargararbitro();//primero se carga el usuario 


            try
            {

                guardararbitro();//se lo pasamos a guardar usuario 
                limpiarCuadros();
                TXT_CODIGO.Enabled = true;
                BuscarArbitros(""); // mandar llamar al data grip con los nuevos datos
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
        }

        private void BTN_ELIMINAR_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Estas seguro que deseas elimiar este registro", "Eliminar Resgistro", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                try
                {
                    Eliminararbitro();
                    BuscarArbitros("");
                }
                catch (Exception ex)
                {

                    MessageBox.Show(ex.Message);
                }
            }
        }

        private void BTN_CANCELAR_Click(object sender, EventArgs e)
        {
            limpiarCuadros();
        }

        private void DGV_ARBITROS_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            TXT_CODIGO.Enabled = false;
            try
            {
                Modificararbitros();
                BuscarArbitros("");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
