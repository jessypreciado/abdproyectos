﻿namespace cl
{
    partial class PartidaP
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PartidaP));
            this.BTN_AGREGAR = new System.Windows.Forms.Button();
            this.BTN_GUARDAR = new System.Windows.Forms.Button();
            this.BTN_ELIMINAR = new System.Windows.Forms.Button();
            this.BTN_CANCELAR = new System.Windows.Forms.Button();
            this.DGV_PARTIDA = new System.Windows.Forms.DataGridView();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.LBL_ID = new System.Windows.Forms.Label();
            this.CMB_MOV = new System.Windows.Forms.ComboBox();
            this.CMB_ARBITRO = new System.Windows.Forms.ComboBox();
            this.CMB_SALA = new System.Windows.Forms.ComboBox();
            this.TXT_ID_PART = new System.Windows.Forms.TextBox();
            this.XT_BUSCARPR = new System.Windows.Forms.TextBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.DGV_PARTIDA)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // BTN_AGREGAR
            // 
            this.BTN_AGREGAR.Location = new System.Drawing.Point(632, 96);
            this.BTN_AGREGAR.Name = "BTN_AGREGAR";
            this.BTN_AGREGAR.Size = new System.Drawing.Size(75, 23);
            this.BTN_AGREGAR.TabIndex = 0;
            this.BTN_AGREGAR.Text = "AGREGAR";
            this.BTN_AGREGAR.UseVisualStyleBackColor = true;
            this.BTN_AGREGAR.Click += new System.EventHandler(this.BTN_AGREGAR_Click);
            // 
            // BTN_GUARDAR
            // 
            this.BTN_GUARDAR.Location = new System.Drawing.Point(632, 126);
            this.BTN_GUARDAR.Name = "BTN_GUARDAR";
            this.BTN_GUARDAR.Size = new System.Drawing.Size(75, 23);
            this.BTN_GUARDAR.TabIndex = 1;
            this.BTN_GUARDAR.Text = "GUARDAR";
            this.BTN_GUARDAR.UseVisualStyleBackColor = true;
            this.BTN_GUARDAR.Click += new System.EventHandler(this.BTN_GUARDAR_Click);
            // 
            // BTN_ELIMINAR
            // 
            this.BTN_ELIMINAR.Location = new System.Drawing.Point(632, 157);
            this.BTN_ELIMINAR.Name = "BTN_ELIMINAR";
            this.BTN_ELIMINAR.Size = new System.Drawing.Size(75, 23);
            this.BTN_ELIMINAR.TabIndex = 2;
            this.BTN_ELIMINAR.Text = "ELIMINAR";
            this.BTN_ELIMINAR.UseVisualStyleBackColor = true;
            this.BTN_ELIMINAR.Click += new System.EventHandler(this.BTN_ELIMINAR_Click);
            // 
            // BTN_CANCELAR
            // 
            this.BTN_CANCELAR.Location = new System.Drawing.Point(632, 186);
            this.BTN_CANCELAR.Name = "BTN_CANCELAR";
            this.BTN_CANCELAR.Size = new System.Drawing.Size(75, 23);
            this.BTN_CANCELAR.TabIndex = 3;
            this.BTN_CANCELAR.Text = "CANCELAR";
            this.BTN_CANCELAR.UseVisualStyleBackColor = true;
            this.BTN_CANCELAR.Click += new System.EventHandler(this.BTN_CANCELAR_Click);
            // 
            // DGV_PARTIDA
            // 
            this.DGV_PARTIDA.BackgroundColor = System.Drawing.Color.SteelBlue;
            this.DGV_PARTIDA.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DGV_PARTIDA.Location = new System.Drawing.Point(63, 246);
            this.DGV_PARTIDA.Name = "DGV_PARTIDA";
            this.DGV_PARTIDA.Size = new System.Drawing.Size(655, 150);
            this.DGV_PARTIDA.TabIndex = 4;
            this.DGV_PARTIDA.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DGV_PARTIDA_CellDoubleClick);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(95, 96);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(71, 13);
            this.label1.TabIndex = 5;
            this.label1.Text = "ID  PARTIDA";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(98, 135);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(76, 13);
            this.label2.TabIndex = 6;
            this.label2.Text = "MOVIMIENTO";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(101, 166);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(55, 13);
            this.label3.TabIndex = 7;
            this.label3.Text = "ARBITRO";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(101, 204);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(34, 13);
            this.label4.TabIndex = 8;
            this.label4.Text = "SALA";
            // 
            // LBL_ID
            // 
            this.LBL_ID.AutoSize = true;
            this.LBL_ID.Location = new System.Drawing.Point(672, 33);
            this.LBL_ID.Name = "LBL_ID";
            this.LBL_ID.Size = new System.Drawing.Size(13, 13);
            this.LBL_ID.TabIndex = 9;
            this.LBL_ID.Text = "0";
            // 
            // CMB_MOV
            // 
            this.CMB_MOV.FormattingEnabled = true;
            this.CMB_MOV.Location = new System.Drawing.Point(191, 126);
            this.CMB_MOV.Name = "CMB_MOV";
            this.CMB_MOV.Size = new System.Drawing.Size(193, 21);
            this.CMB_MOV.TabIndex = 10;
            this.CMB_MOV.Click += new System.EventHandler(this.CMB_MOV_Click);
            // 
            // CMB_ARBITRO
            // 
            this.CMB_ARBITRO.FormattingEnabled = true;
            this.CMB_ARBITRO.Location = new System.Drawing.Point(191, 157);
            this.CMB_ARBITRO.Name = "CMB_ARBITRO";
            this.CMB_ARBITRO.Size = new System.Drawing.Size(193, 21);
            this.CMB_ARBITRO.TabIndex = 11;
            this.CMB_ARBITRO.SelectedIndexChanged += new System.EventHandler(this.CMB_ARBITRO_SelectedIndexChanged);
            this.CMB_ARBITRO.Click += new System.EventHandler(this.CMB_ARBITRO_Click);
            // 
            // CMB_SALA
            // 
            this.CMB_SALA.FormattingEnabled = true;
            this.CMB_SALA.Location = new System.Drawing.Point(191, 196);
            this.CMB_SALA.Name = "CMB_SALA";
            this.CMB_SALA.Size = new System.Drawing.Size(193, 21);
            this.CMB_SALA.TabIndex = 12;
            this.CMB_SALA.Click += new System.EventHandler(this.CMB_SALA_Click);
            // 
            // TXT_ID_PART
            // 
            this.TXT_ID_PART.Location = new System.Drawing.Point(191, 81);
            this.TXT_ID_PART.Name = "TXT_ID_PART";
            this.TXT_ID_PART.Size = new System.Drawing.Size(193, 20);
            this.TXT_ID_PART.TabIndex = 13;
            // 
            // XT_BUSCARPR
            // 
            this.XT_BUSCARPR.Location = new System.Drawing.Point(63, 26);
            this.XT_BUSCARPR.Name = "XT_BUSCARPR";
            this.XT_BUSCARPR.Size = new System.Drawing.Size(321, 20);
            this.XT_BUSCARPR.TabIndex = 14;
            this.XT_BUSCARPR.TextChanged += new System.EventHandler(this.XT_BUSCARPR_TextChanged);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(402, 24);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(26, 22);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 48;
            this.pictureBox1.TabStop = false;
            // 
            // PartidaP
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.SteelBlue;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.XT_BUSCARPR);
            this.Controls.Add(this.TXT_ID_PART);
            this.Controls.Add(this.CMB_SALA);
            this.Controls.Add(this.CMB_ARBITRO);
            this.Controls.Add(this.CMB_MOV);
            this.Controls.Add(this.LBL_ID);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.DGV_PARTIDA);
            this.Controls.Add(this.BTN_CANCELAR);
            this.Controls.Add(this.BTN_ELIMINAR);
            this.Controls.Add(this.BTN_GUARDAR);
            this.Controls.Add(this.BTN_AGREGAR);
            this.Name = "PartidaP";
            this.Text = "Partida";
            this.Load += new System.EventHandler(this.PartidaP_Load);
            ((System.ComponentModel.ISupportInitialize)(this.DGV_PARTIDA)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button BTN_AGREGAR;
        private System.Windows.Forms.Button BTN_GUARDAR;
        private System.Windows.Forms.Button BTN_ELIMINAR;
        private System.Windows.Forms.Button BTN_CANCELAR;
        private System.Windows.Forms.DataGridView DGV_PARTIDA;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label LBL_ID;
        private System.Windows.Forms.ComboBox CMB_MOV;
        private System.Windows.Forms.ComboBox CMB_ARBITRO;
        private System.Windows.Forms.ComboBox CMB_SALA;
        private System.Windows.Forms.TextBox TXT_ID_PART;
        private System.Windows.Forms.TextBox XT_BUSCARPR;
        private System.Windows.Forms.PictureBox pictureBox1;
    }
}