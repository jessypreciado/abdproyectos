﻿namespace cl
{
    partial class JugadoresP
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(JugadoresP));
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.TXT_CODIGO = new System.Windows.Forms.TextBox();
            this.TXT_NOMBRE = new System.Windows.Forms.TextBox();
            this.TXT_DIRECCION = new System.Windows.Forms.TextBox();
            this.TXT_TELEFONO = new System.Windows.Forms.TextBox();
            this.TXT_NIVELJUEGO = new System.Windows.Forms.TextBox();
            this.CMB_PAIS = new System.Windows.Forms.ComboBox();
            this.txt_buscar = new System.Windows.Forms.TextBox();
            this.BTN_AGREGAR = new System.Windows.Forms.Button();
            this.BTN_GUARDAR = new System.Windows.Forms.Button();
            this.BTN_ELIMINAR = new System.Windows.Forms.Button();
            this.BTN_CANCELAR = new System.Windows.Forms.Button();
            this.DGV_JUGADORES = new System.Windows.Forms.DataGridView();
            this.LBL_ID = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DGV_JUGADORES)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(308, 40);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(26, 22);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 62;
            this.pictureBox1.TabStop = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Berlin Sans FB", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(44, 100);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(51, 13);
            this.label1.TabIndex = 63;
            this.label1.Text = "CODIGO";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Berlin Sans FB", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(54, 126);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(54, 13);
            this.label2.TabIndex = 64;
            this.label2.Text = "NOMBRE";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Berlin Sans FB", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(42, 156);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(66, 13);
            this.label3.TabIndex = 65;
            this.label3.Text = "DIRECCION";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Berlin Sans FB", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(328, 154);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(64, 13);
            this.label4.TabIndex = 66;
            this.label4.Text = "TELEFONO";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Berlin Sans FB", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(361, 108);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(31, 13);
            this.label6.TabIndex = 68;
            this.label6.Text = "PAIS";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Berlin Sans FB", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(311, 134);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(92, 13);
            this.label7.TabIndex = 69;
            this.label7.Text = "NIVEL DE JUEGO";
            // 
            // TXT_CODIGO
            // 
            this.TXT_CODIGO.Location = new System.Drawing.Point(128, 100);
            this.TXT_CODIGO.Name = "TXT_CODIGO";
            this.TXT_CODIGO.Size = new System.Drawing.Size(174, 20);
            this.TXT_CODIGO.TabIndex = 70;
            // 
            // TXT_NOMBRE
            // 
            this.TXT_NOMBRE.Location = new System.Drawing.Point(128, 126);
            this.TXT_NOMBRE.Name = "TXT_NOMBRE";
            this.TXT_NOMBRE.Size = new System.Drawing.Size(174, 20);
            this.TXT_NOMBRE.TabIndex = 71;
            this.TXT_NOMBRE.TextChanged += new System.EventHandler(this.textBox2_TextChanged);
            // 
            // TXT_DIRECCION
            // 
            this.TXT_DIRECCION.Location = new System.Drawing.Point(128, 153);
            this.TXT_DIRECCION.Name = "TXT_DIRECCION";
            this.TXT_DIRECCION.Size = new System.Drawing.Size(174, 20);
            this.TXT_DIRECCION.TabIndex = 72;
            // 
            // TXT_TELEFONO
            // 
            this.TXT_TELEFONO.Location = new System.Drawing.Point(425, 157);
            this.TXT_TELEFONO.Name = "TXT_TELEFONO";
            this.TXT_TELEFONO.Size = new System.Drawing.Size(140, 20);
            this.TXT_TELEFONO.TabIndex = 73;
            // 
            // TXT_NIVELJUEGO
            // 
            this.TXT_NIVELJUEGO.Location = new System.Drawing.Point(425, 127);
            this.TXT_NIVELJUEGO.Name = "TXT_NIVELJUEGO";
            this.TXT_NIVELJUEGO.Size = new System.Drawing.Size(140, 20);
            this.TXT_NIVELJUEGO.TabIndex = 75;
            // 
            // CMB_PAIS
            // 
            this.CMB_PAIS.FormattingEnabled = true;
            this.CMB_PAIS.Location = new System.Drawing.Point(425, 100);
            this.CMB_PAIS.Name = "CMB_PAIS";
            this.CMB_PAIS.Size = new System.Drawing.Size(140, 21);
            this.CMB_PAIS.TabIndex = 76;
            this.CMB_PAIS.SelectedIndexChanged += new System.EventHandler(this.CMB_PAIS_SelectedIndexChanged);
            this.CMB_PAIS.Click += new System.EventHandler(this.CMB_PAIS_Click);
            // 
            // txt_buscar
            // 
            this.txt_buscar.Location = new System.Drawing.Point(45, 40);
            this.txt_buscar.Name = "txt_buscar";
            this.txt_buscar.Size = new System.Drawing.Size(257, 20);
            this.txt_buscar.TabIndex = 77;
            this.txt_buscar.TextChanged += new System.EventHandler(this.textBox5_TextChanged);
            // 
            // BTN_AGREGAR
            // 
            this.BTN_AGREGAR.Location = new System.Drawing.Point(611, 93);
            this.BTN_AGREGAR.Name = "BTN_AGREGAR";
            this.BTN_AGREGAR.Size = new System.Drawing.Size(75, 23);
            this.BTN_AGREGAR.TabIndex = 78;
            this.BTN_AGREGAR.Text = "AGREGAR";
            this.BTN_AGREGAR.UseVisualStyleBackColor = true;
            this.BTN_AGREGAR.Click += new System.EventHandler(this.BTN_AGREGAR_Click);
            // 
            // BTN_GUARDAR
            // 
            this.BTN_GUARDAR.Location = new System.Drawing.Point(611, 122);
            this.BTN_GUARDAR.Name = "BTN_GUARDAR";
            this.BTN_GUARDAR.Size = new System.Drawing.Size(75, 23);
            this.BTN_GUARDAR.TabIndex = 79;
            this.BTN_GUARDAR.Text = "GUARDAR";
            this.BTN_GUARDAR.UseVisualStyleBackColor = true;
            this.BTN_GUARDAR.Click += new System.EventHandler(this.BTN_GUARDAR_Click);
            // 
            // BTN_ELIMINAR
            // 
            this.BTN_ELIMINAR.Location = new System.Drawing.Point(611, 150);
            this.BTN_ELIMINAR.Name = "BTN_ELIMINAR";
            this.BTN_ELIMINAR.Size = new System.Drawing.Size(75, 23);
            this.BTN_ELIMINAR.TabIndex = 80;
            this.BTN_ELIMINAR.Text = "ELIMINAR";
            this.BTN_ELIMINAR.UseVisualStyleBackColor = true;
            this.BTN_ELIMINAR.Click += new System.EventHandler(this.BTN_ELIMINAR_Click);
            // 
            // BTN_CANCELAR
            // 
            this.BTN_CANCELAR.Location = new System.Drawing.Point(611, 182);
            this.BTN_CANCELAR.Name = "BTN_CANCELAR";
            this.BTN_CANCELAR.Size = new System.Drawing.Size(75, 23);
            this.BTN_CANCELAR.TabIndex = 81;
            this.BTN_CANCELAR.Text = "CANCELAR";
            this.BTN_CANCELAR.UseVisualStyleBackColor = true;
            this.BTN_CANCELAR.Click += new System.EventHandler(this.BTN_CANCELAR_Click);
            // 
            // DGV_JUGADORES
            // 
            this.DGV_JUGADORES.BackgroundColor = System.Drawing.Color.SteelBlue;
            this.DGV_JUGADORES.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DGV_JUGADORES.Location = new System.Drawing.Point(57, 247);
            this.DGV_JUGADORES.Name = "DGV_JUGADORES";
            this.DGV_JUGADORES.Size = new System.Drawing.Size(629, 150);
            this.DGV_JUGADORES.TabIndex = 82;
            this.DGV_JUGADORES.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DGV_JUGADORES_CellDoubleClick);
            // 
            // LBL_ID
            // 
            this.LBL_ID.AutoSize = true;
            this.LBL_ID.Location = new System.Drawing.Point(672, 49);
            this.LBL_ID.Name = "LBL_ID";
            this.LBL_ID.Size = new System.Drawing.Size(13, 13);
            this.LBL_ID.TabIndex = 83;
            this.LBL_ID.Text = "0";
            // 
            // JugadoresP
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.SteelBlue;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.LBL_ID);
            this.Controls.Add(this.DGV_JUGADORES);
            this.Controls.Add(this.BTN_CANCELAR);
            this.Controls.Add(this.BTN_ELIMINAR);
            this.Controls.Add(this.BTN_GUARDAR);
            this.Controls.Add(this.BTN_AGREGAR);
            this.Controls.Add(this.txt_buscar);
            this.Controls.Add(this.CMB_PAIS);
            this.Controls.Add(this.TXT_NIVELJUEGO);
            this.Controls.Add(this.TXT_TELEFONO);
            this.Controls.Add(this.TXT_DIRECCION);
            this.Controls.Add(this.TXT_NOMBRE);
            this.Controls.Add(this.TXT_CODIGO);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.pictureBox1);
            this.Name = "JugadoresP";
            this.Text = "JugadoresP";
            this.Load += new System.EventHandler(this.JugadoresP_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DGV_JUGADORES)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox TXT_CODIGO;
        private System.Windows.Forms.TextBox TXT_NOMBRE;
        private System.Windows.Forms.TextBox TXT_DIRECCION;
        private System.Windows.Forms.TextBox TXT_TELEFONO;
        private System.Windows.Forms.TextBox TXT_NIVELJUEGO;
        private System.Windows.Forms.ComboBox CMB_PAIS;
        private System.Windows.Forms.TextBox txt_buscar;
        private System.Windows.Forms.Button BTN_AGREGAR;
        private System.Windows.Forms.Button BTN_GUARDAR;
        private System.Windows.Forms.Button BTN_ELIMINAR;
        private System.Windows.Forms.Button BTN_CANCELAR;
        private System.Windows.Forms.DataGridView DGV_JUGADORES;
        private System.Windows.Forms.Label LBL_ID;
    }
}