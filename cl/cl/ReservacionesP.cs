﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CLEntidad;
using CLLogica;

namespace cl
{
    public partial class ReservacionesP : Form
    {
        ReservacionesE _reservacionesE= new ReservacionesE();
        RservacionesL _reservacionesL = new RservacionesL();

        HotelL _hotelL = new HotelL();
        ParticipantesL _participantesL = new ParticipantesL();
        VReservacionL _vReservacionL = new VReservacionL();
 
        public ReservacionesP()
        {
            InitializeComponent();
            _participantesL = new ParticipantesL();
            _reservacionesL = new RservacionesL();
            _reservacionesE = new ReservacionesE();
            _hotelL = new HotelL();
        }

        private void ReservacionesP_Load(object sender, EventArgs e)
        {
            BuscarReservacion("");
        }
        private void limpiarCuadros()
        {
          LBL_RES.Text = "0";
          CMB_HOTEL.Text = "";
          CMB_PART.Text = "";
          DTP_ENTRADA.Text = "";
          DTP_SALIDA.Text = "";

        }
        private void BuscarReservacion(string filtro)
        {
            DGV_RESERVACIONES.DataSource = _vReservacionL.GetVRser(filtro);//pasar la lista 
        }
        private void ModificarReservaciones()//funcion para  modificar  la tabla 
        {
            LBL_RES.Text = DGV_RESERVACIONES.CurrentRow.Cells["RESERVACION"].Value.ToString();
            DTP_ENTRADA.Text = DGV_RESERVACIONES.CurrentRow.Cells["fechaentrada"].Value.ToString();
            DTP_SALIDA.Text = DGV_RESERVACIONES.CurrentRow.Cells["fechasalida"].Value.ToString();
            CMB_HOTEL.Text = DGV_RESERVACIONES.CurrentRow.Cells["HOTEL"].Value.ToString();
            CMB_PART.Text = DGV_RESERVACIONES.CurrentRow.Cells["participante"].Value.ToString();


        }
        private void EliminarReservacion()//necesitamos el id int para que elimine el dato
        {
            var id = DGV_RESERVACIONES.CurrentRow.Cells["id_reservacion"].Value;//DEPENDE DEL DQATA
            _reservacionesL.eliminar(Convert.ToInt32(id));//EL METODO LO NECESITA INT
        }
        private void guardarReservaciones()
        {
           _reservacionesL.guardar(_reservacionesE);
        }
        private void CargarReservaciones()//crear global el objeto para usarlo en otro lugar 
        {
            DataSet dAL;
            dAL = _hotelL.GetidHotel(CMB_HOTEL.Text);
            DataSet dlL;
            dlL = _participantesL.GetiParticipantes(CMB_PART.Text);

            _reservacionesE.Id_reservacion = Convert.ToInt32(LBL_RES.Text);
            _reservacionesE.Fechaentrada = DTP_ENTRADA.Text;
            _reservacionesE.Fechasalida = DTP_SALIDA.Text;
            _reservacionesE.Id_hotel = Convert.ToInt32(dAL.Tables[0].Rows[0]["id_hotel"].ToString()); 
            _reservacionesE.Idparticipante = dlL.Tables[0].Rows[0]["id_participante"].ToString(); 
        }
        private void TraerHotel(string filtro)
        {
            CMB_HOTEL.DataSource = _hotelL.GetHoteles(filtro);
            CMB_HOTEL.DisplayMember = "nombre";
        }
        private void TraerPart(string filtro)
        {
            CMB_PART.DataSource =_participantesL.GetParticipantes(filtro);
            CMB_PART.DisplayMember = "nombre";
        }
        private void CMB_HOTEL_Click(object sender, EventArgs e)
        {
            TraerHotel("");
        }

        private void CMB_PART_Click(object sender, EventArgs e)
        {
            TraerPart("");
        }

        private void TXT_BUSCAR_TextChanged(object sender, EventArgs e)
        {
            BuscarReservacion(TXT_BUSCAR.Text);
        }

        private void BTN_AGREGAR_Click(object sender, EventArgs e)
        {
            limpiarCuadros();
            CMB_HOTEL.Focus();
        }

        private void BTN_GUARDAR_Click(object sender, EventArgs e)
        {
            CargarReservaciones();
            try
            {

                guardarReservaciones();//se lo pasamos a guardar usuario 
                limpiarCuadros();
                BuscarReservacion(""); // mandar llamar al data grip con los nuevos datos
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
        }

        private void BTN_ELIMINAR_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Estas seguro que deseas elimiar este registro", "Eliminar Resgistro", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                try
                {
                    EliminarReservacion();
                    BuscarReservacion("");
                }
                catch (Exception ex)
                {

                    MessageBox.Show(ex.Message);
                }
            }
        }

        private void BTN_CANCELAR_Click(object sender, EventArgs e)
        {
            limpiarCuadros();
        }

        private void DGV_RESERVACIONES_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                ModificarReservaciones();
                BuscarReservacion("");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
