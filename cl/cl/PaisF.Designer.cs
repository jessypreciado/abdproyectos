﻿namespace cl
{
    partial class PaisF
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PaisF));
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.BTN_AGREGARP = new System.Windows.Forms.Button();
            this.BTN_GUARDARP = new System.Windows.Forms.Button();
            this.BTN_ELIMINARP = new System.Windows.Forms.Button();
            this.BTN_CANCELARP = new System.Windows.Forms.Button();
            this.TXT_BUSCRAP = new System.Windows.Forms.TextBox();
            this.TXT_NOMBREP = new System.Windows.Forms.TextBox();
            this.TXT_NUMP = new System.Windows.Forms.TextBox();
            this.DGV_PAISES = new System.Windows.Forms.DataGridView();
            this.LBL_IDP = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.CMB_REPRESENTAP = new System.Windows.Forms.ComboBox();
            ((System.ComponentModel.ISupportInitialize)(this.DGV_PAISES)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Berlin Sans FB", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(45, 192);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(75, 13);
            this.label4.TabIndex = 28;
            this.label4.Text = "REPRESENTA";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Berlin Sans FB", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(68, 160);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(58, 13);
            this.label3.TabIndex = 27;
            this.label3.Text = "N° CLUBS";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Berlin Sans FB", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(71, 127);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(54, 13);
            this.label2.TabIndex = 26;
            this.label2.Text = "NOMBRE";
            // 
            // BTN_AGREGARP
            // 
            this.BTN_AGREGARP.Font = new System.Drawing.Font("Berlin Sans FB", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BTN_AGREGARP.Location = new System.Drawing.Point(559, 117);
            this.BTN_AGREGARP.Name = "BTN_AGREGARP";
            this.BTN_AGREGARP.Size = new System.Drawing.Size(75, 23);
            this.BTN_AGREGARP.TabIndex = 29;
            this.BTN_AGREGARP.Text = "AGREGAR";
            this.BTN_AGREGARP.UseVisualStyleBackColor = true;
            this.BTN_AGREGARP.Click += new System.EventHandler(this.BTN_AGREGARP_Click);
            // 
            // BTN_GUARDARP
            // 
            this.BTN_GUARDARP.Font = new System.Drawing.Font("Berlin Sans FB", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BTN_GUARDARP.Location = new System.Drawing.Point(559, 147);
            this.BTN_GUARDARP.Name = "BTN_GUARDARP";
            this.BTN_GUARDARP.Size = new System.Drawing.Size(75, 23);
            this.BTN_GUARDARP.TabIndex = 30;
            this.BTN_GUARDARP.Text = "GUARDAR";
            this.BTN_GUARDARP.UseVisualStyleBackColor = true;
            this.BTN_GUARDARP.Click += new System.EventHandler(this.BTN_GUARDARP_Click);
            // 
            // BTN_ELIMINARP
            // 
            this.BTN_ELIMINARP.Font = new System.Drawing.Font("Berlin Sans FB", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BTN_ELIMINARP.Location = new System.Drawing.Point(559, 176);
            this.BTN_ELIMINARP.Name = "BTN_ELIMINARP";
            this.BTN_ELIMINARP.Size = new System.Drawing.Size(75, 23);
            this.BTN_ELIMINARP.TabIndex = 31;
            this.BTN_ELIMINARP.Text = "ELIMINAR";
            this.BTN_ELIMINARP.UseVisualStyleBackColor = true;
            this.BTN_ELIMINARP.Click += new System.EventHandler(this.BTN_ELIMINARP_Click);
            // 
            // BTN_CANCELARP
            // 
            this.BTN_CANCELARP.Font = new System.Drawing.Font("Berlin Sans FB", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BTN_CANCELARP.Location = new System.Drawing.Point(559, 205);
            this.BTN_CANCELARP.Name = "BTN_CANCELARP";
            this.BTN_CANCELARP.Size = new System.Drawing.Size(75, 23);
            this.BTN_CANCELARP.TabIndex = 32;
            this.BTN_CANCELARP.Text = "CANCELAR";
            this.BTN_CANCELARP.UseVisualStyleBackColor = true;
            this.BTN_CANCELARP.Click += new System.EventHandler(this.BTN_CANCELARP_Click);
            // 
            // TXT_BUSCRAP
            // 
            this.TXT_BUSCRAP.Location = new System.Drawing.Point(48, 33);
            this.TXT_BUSCRAP.Name = "TXT_BUSCRAP";
            this.TXT_BUSCRAP.Size = new System.Drawing.Size(305, 20);
            this.TXT_BUSCRAP.TabIndex = 33;
            this.TXT_BUSCRAP.TextChanged += new System.EventHandler(this.TXT_BUSCRAP_TextChanged);
            // 
            // TXT_NOMBREP
            // 
            this.TXT_NOMBREP.Font = new System.Drawing.Font("Berlin Sans FB", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TXT_NOMBREP.Location = new System.Drawing.Point(144, 119);
            this.TXT_NOMBREP.Name = "TXT_NOMBREP";
            this.TXT_NOMBREP.Size = new System.Drawing.Size(209, 21);
            this.TXT_NOMBREP.TabIndex = 42;
            // 
            // TXT_NUMP
            // 
            this.TXT_NUMP.Font = new System.Drawing.Font("Berlin Sans FB", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TXT_NUMP.Location = new System.Drawing.Point(144, 160);
            this.TXT_NUMP.Name = "TXT_NUMP";
            this.TXT_NUMP.Size = new System.Drawing.Size(209, 21);
            this.TXT_NUMP.TabIndex = 43;
            // 
            // DGV_PAISES
            // 
            this.DGV_PAISES.BackgroundColor = System.Drawing.Color.SteelBlue;
            this.DGV_PAISES.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DGV_PAISES.Location = new System.Drawing.Point(48, 251);
            this.DGV_PAISES.Name = "DGV_PAISES";
            this.DGV_PAISES.Size = new System.Drawing.Size(586, 150);
            this.DGV_PAISES.TabIndex = 45;
            this.DGV_PAISES.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DGV_PAISES_CellDoubleClick);
            // 
            // LBL_IDP
            // 
            this.LBL_IDP.AutoSize = true;
            this.LBL_IDP.Location = new System.Drawing.Point(392, 42);
            this.LBL_IDP.Name = "LBL_IDP";
            this.LBL_IDP.Size = new System.Drawing.Size(13, 13);
            this.LBL_IDP.TabIndex = 46;
            this.LBL_IDP.Text = "0";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(379, 33);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(26, 22);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 47;
            this.pictureBox1.TabStop = false;
            // 
            // CMB_REPRESENTAP
            // 
            this.CMB_REPRESENTAP.FormattingEnabled = true;
            this.CMB_REPRESENTAP.Location = new System.Drawing.Point(144, 192);
            this.CMB_REPRESENTAP.Name = "CMB_REPRESENTAP";
            this.CMB_REPRESENTAP.Size = new System.Drawing.Size(209, 21);
            this.CMB_REPRESENTAP.TabIndex = 48;
            this.CMB_REPRESENTAP.Click += new System.EventHandler(this.CMB_REPRESENTAP_Click);
            // 
            // PaisF
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.SteelBlue;
            this.ClientSize = new System.Drawing.Size(644, 450);
            this.Controls.Add(this.CMB_REPRESENTAP);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.LBL_IDP);
            this.Controls.Add(this.DGV_PAISES);
            this.Controls.Add(this.TXT_NUMP);
            this.Controls.Add(this.TXT_NOMBREP);
            this.Controls.Add(this.TXT_BUSCRAP);
            this.Controls.Add(this.BTN_CANCELARP);
            this.Controls.Add(this.BTN_ELIMINARP);
            this.Controls.Add(this.BTN_GUARDARP);
            this.Controls.Add(this.BTN_AGREGARP);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Name = "PaisF";
            this.Text = "PaisF";
            this.Load += new System.EventHandler(this.PaisF_Load);
            ((System.ComponentModel.ISupportInitialize)(this.DGV_PAISES)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button BTN_AGREGARP;
        private System.Windows.Forms.Button BTN_GUARDARP;
        private System.Windows.Forms.Button BTN_ELIMINARP;
        private System.Windows.Forms.Button BTN_CANCELARP;
        private System.Windows.Forms.TextBox TXT_BUSCRAP;
        private System.Windows.Forms.TextBox TXT_NOMBREP;
        private System.Windows.Forms.TextBox TXT_NUMP;
        private System.Windows.Forms.DataGridView DGV_PAISES;
        private System.Windows.Forms.Label LBL_IDP;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.ComboBox CMB_REPRESENTAP;
    }
}