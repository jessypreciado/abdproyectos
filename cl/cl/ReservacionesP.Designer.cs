﻿namespace cl
{
    partial class ReservacionesP
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ReservacionesP));
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.TXT_BUSCAR = new System.Windows.Forms.TextBox();
            this.LBL_RES = new System.Windows.Forms.Label();
            this.BTN_AGREGAR = new System.Windows.Forms.Button();
            this.BTN_GUARDAR = new System.Windows.Forms.Button();
            this.BTN_ELIMINAR = new System.Windows.Forms.Button();
            this.BTN_CANCELAR = new System.Windows.Forms.Button();
            this.DGV_RESERVACIONES = new System.Windows.Forms.DataGridView();
            this.CMB_HOTEL = new System.Windows.Forms.ComboBox();
            this.CMB_PART = new System.Windows.Forms.ComboBox();
            this.DTP_ENTRADA = new System.Windows.Forms.DateTimePicker();
            this.DTP_SALIDA = new System.Windows.Forms.DateTimePicker();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.DGV_RESERVACIONES)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Berlin Sans FB", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(74, 231);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(46, 13);
            this.label5.TabIndex = 48;
            this.label5.Text = "SALIDA";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Berlin Sans FB", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(61, 190);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(59, 13);
            this.label4.TabIndex = 47;
            this.label4.Text = "ENTRADA";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Berlin Sans FB", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(36, 147);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(84, 13);
            this.label3.TabIndex = 46;
            this.label3.Text = "PARTICIPANTE";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Berlin Sans FB", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(77, 112);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(43, 13);
            this.label2.TabIndex = 45;
            this.label2.Text = "HOTEL";
            // 
            // TXT_BUSCAR
            // 
            this.TXT_BUSCAR.Location = new System.Drawing.Point(39, 25);
            this.TXT_BUSCAR.Name = "TXT_BUSCAR";
            this.TXT_BUSCAR.Size = new System.Drawing.Size(313, 20);
            this.TXT_BUSCAR.TabIndex = 50;
            this.TXT_BUSCAR.TextChanged += new System.EventHandler(this.TXT_BUSCAR_TextChanged);
            // 
            // LBL_RES
            // 
            this.LBL_RES.AutoSize = true;
            this.LBL_RES.Location = new System.Drawing.Point(374, 32);
            this.LBL_RES.Name = "LBL_RES";
            this.LBL_RES.Size = new System.Drawing.Size(13, 13);
            this.LBL_RES.TabIndex = 51;
            this.LBL_RES.Text = "0";
            // 
            // BTN_AGREGAR
            // 
            this.BTN_AGREGAR.Location = new System.Drawing.Point(679, 76);
            this.BTN_AGREGAR.Name = "BTN_AGREGAR";
            this.BTN_AGREGAR.Size = new System.Drawing.Size(75, 23);
            this.BTN_AGREGAR.TabIndex = 52;
            this.BTN_AGREGAR.Text = "AGREGAR";
            this.BTN_AGREGAR.UseVisualStyleBackColor = true;
            this.BTN_AGREGAR.Click += new System.EventHandler(this.BTN_AGREGAR_Click);
            // 
            // BTN_GUARDAR
            // 
            this.BTN_GUARDAR.Location = new System.Drawing.Point(679, 102);
            this.BTN_GUARDAR.Name = "BTN_GUARDAR";
            this.BTN_GUARDAR.Size = new System.Drawing.Size(75, 23);
            this.BTN_GUARDAR.TabIndex = 53;
            this.BTN_GUARDAR.Text = "GUARDAR";
            this.BTN_GUARDAR.UseVisualStyleBackColor = true;
            this.BTN_GUARDAR.Click += new System.EventHandler(this.BTN_GUARDAR_Click);
            // 
            // BTN_ELIMINAR
            // 
            this.BTN_ELIMINAR.Location = new System.Drawing.Point(679, 131);
            this.BTN_ELIMINAR.Name = "BTN_ELIMINAR";
            this.BTN_ELIMINAR.Size = new System.Drawing.Size(75, 23);
            this.BTN_ELIMINAR.TabIndex = 54;
            this.BTN_ELIMINAR.Text = "ELIMINAR";
            this.BTN_ELIMINAR.UseVisualStyleBackColor = true;
            this.BTN_ELIMINAR.Click += new System.EventHandler(this.BTN_ELIMINAR_Click);
            // 
            // BTN_CANCELAR
            // 
            this.BTN_CANCELAR.Location = new System.Drawing.Point(679, 160);
            this.BTN_CANCELAR.Name = "BTN_CANCELAR";
            this.BTN_CANCELAR.Size = new System.Drawing.Size(75, 23);
            this.BTN_CANCELAR.TabIndex = 55;
            this.BTN_CANCELAR.Text = "CANCELAR";
            this.BTN_CANCELAR.UseVisualStyleBackColor = true;
            this.BTN_CANCELAR.Click += new System.EventHandler(this.BTN_CANCELAR_Click);
            // 
            // DGV_RESERVACIONES
            // 
            this.DGV_RESERVACIONES.BackgroundColor = System.Drawing.Color.SteelBlue;
            this.DGV_RESERVACIONES.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DGV_RESERVACIONES.Location = new System.Drawing.Point(39, 271);
            this.DGV_RESERVACIONES.Name = "DGV_RESERVACIONES";
            this.DGV_RESERVACIONES.Size = new System.Drawing.Size(737, 150);
            this.DGV_RESERVACIONES.TabIndex = 56;
            this.DGV_RESERVACIONES.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DGV_RESERVACIONES_CellDoubleClick);
            // 
            // CMB_HOTEL
            // 
            this.CMB_HOTEL.FormattingEnabled = true;
            this.CMB_HOTEL.Location = new System.Drawing.Point(148, 112);
            this.CMB_HOTEL.Name = "CMB_HOTEL";
            this.CMB_HOTEL.Size = new System.Drawing.Size(200, 21);
            this.CMB_HOTEL.TabIndex = 57;
            this.CMB_HOTEL.Click += new System.EventHandler(this.CMB_HOTEL_Click);
            // 
            // CMB_PART
            // 
            this.CMB_PART.FormattingEnabled = true;
            this.CMB_PART.Location = new System.Drawing.Point(148, 147);
            this.CMB_PART.Name = "CMB_PART";
            this.CMB_PART.Size = new System.Drawing.Size(200, 21);
            this.CMB_PART.TabIndex = 58;
            this.CMB_PART.Click += new System.EventHandler(this.CMB_PART_Click);
            // 
            // DTP_ENTRADA
            // 
            this.DTP_ENTRADA.Location = new System.Drawing.Point(148, 182);
            this.DTP_ENTRADA.Name = "DTP_ENTRADA";
            this.DTP_ENTRADA.Size = new System.Drawing.Size(200, 20);
            this.DTP_ENTRADA.TabIndex = 59;
            // 
            // DTP_SALIDA
            // 
            this.DTP_SALIDA.Location = new System.Drawing.Point(148, 223);
            this.DTP_SALIDA.Name = "DTP_SALIDA";
            this.DTP_SALIDA.Size = new System.Drawing.Size(200, 20);
            this.DTP_SALIDA.TabIndex = 60;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(361, 25);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(26, 22);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 61;
            this.pictureBox1.TabStop = false;
            // 
            // ReservacionesP
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.SteelBlue;
            this.ClientSize = new System.Drawing.Size(799, 450);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.DTP_SALIDA);
            this.Controls.Add(this.DTP_ENTRADA);
            this.Controls.Add(this.CMB_PART);
            this.Controls.Add(this.CMB_HOTEL);
            this.Controls.Add(this.DGV_RESERVACIONES);
            this.Controls.Add(this.BTN_CANCELAR);
            this.Controls.Add(this.BTN_ELIMINAR);
            this.Controls.Add(this.BTN_GUARDAR);
            this.Controls.Add(this.BTN_AGREGAR);
            this.Controls.Add(this.LBL_RES);
            this.Controls.Add(this.TXT_BUSCAR);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Name = "ReservacionesP";
            this.Text = "ReservacionesP";
            this.Load += new System.EventHandler(this.ReservacionesP_Load);
            ((System.ComponentModel.ISupportInitialize)(this.DGV_RESERVACIONES)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox TXT_BUSCAR;
        private System.Windows.Forms.Label LBL_RES;
        private System.Windows.Forms.Button BTN_AGREGAR;
        private System.Windows.Forms.Button BTN_GUARDAR;
        private System.Windows.Forms.Button BTN_ELIMINAR;
        private System.Windows.Forms.Button BTN_CANCELAR;
        private System.Windows.Forms.DataGridView DGV_RESERVACIONES;
        private System.Windows.Forms.ComboBox CMB_HOTEL;
        private System.Windows.Forms.ComboBox CMB_PART;
        private System.Windows.Forms.DateTimePicker DTP_ENTRADA;
        private System.Windows.Forms.DateTimePicker DTP_SALIDA;
        private System.Windows.Forms.PictureBox pictureBox1;
    }
}