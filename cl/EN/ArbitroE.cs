﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CLEntidad
{
    public  class ArbitroE
    {
        private int _id;
        private string _id_participante;
        private string _nombre;
        private string _direccion;
        private string _telefono;
        private string _pais;

        public int Id { get => _id; set => _id = value; }
        public string Id_participante { get => _id_participante; set => _id_participante = value; }
        public string Nombre { get => _nombre; set => _nombre = value; }
        public string Direccion { get => _direccion; set => _direccion = value; }
        public string Telefono { get => _telefono; set => _telefono = value; }
        public string Pais { get => _pais; set => _pais = value; }
    }
}
