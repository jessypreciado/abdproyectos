﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CLEntidad
{
    public class ReservacionesE
    {
        private int _id_reservacion;
        private string _fechaentrada;
        private string _fechasalida;
        private int _id_hotel;
        private string _idparticipante;

        public int Id_reservacion { get => _id_reservacion; set => _id_reservacion = value; }
        public string Fechaentrada { get => _fechaentrada; set => _fechaentrada = value; }
        public string Fechasalida { get => _fechasalida; set => _fechasalida = value; }
        public int Id_hotel { get => _id_hotel; set => _id_hotel = value; }
        public string Idparticipante { get => _idparticipante; set => _idparticipante = value; }
    }
}
