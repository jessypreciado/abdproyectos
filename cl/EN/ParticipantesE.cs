﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CLEntidad
{
    public class ParticipantesE
    {
        private int _id;
        private string  _id_participante;
        private string _nombre;
        private string _direccion;
        private string _telefono;
        private int _tipo;
        private int _id_pais;
        private int _niveljuego;

        private String _pais;


        public int Id { get => _id; set => _id = value; }
        public string Id_participante { get => _id_participante; set => _id_participante = value; }
        public string Nombre { get => _nombre; set => _nombre = value; }
        public string Direccion { get => _direccion; set => _direccion = value; }
        public string Telefono { get => _telefono; set => _telefono = value; }
        public int Tipo { get => _tipo; set => _tipo = value; }
        public int Id_pais { get => _id_pais; set => _id_pais = value; }
        public int Niveljuego { get => _niveljuego; set => _niveljuego = value; }
        public string Pais { get => _pais; set => _pais = value; }
    }
}
