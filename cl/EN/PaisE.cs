﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CLEntidad
{
    public class PaisE
    {
        private int _idpais;
        private string _nombre;
        private int _cantclub;
        private string id_paisr;

        public int Idpais { get => _idpais; set => _idpais = value; }
        public string Nombre { get => _nombre; set => _nombre = value; }
        public int Cantclub { get => _cantclub; set => _cantclub = value; }
        public string Id_paisr { get => id_paisr; set => id_paisr = value; }
    }
}
