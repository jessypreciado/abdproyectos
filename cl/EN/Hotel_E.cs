﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CLEntidad
{
    public class Hotel_E
    {
        private int _id_hotel;
        private string _nombre;
        private string _direccion;

        private string _telefono;

        public int Id_hotel { get => _id_hotel; set => _id_hotel = value; }
        public string Nombre { get => _nombre; set => _nombre = value; }
        public string Direccion { get => _direccion; set => _direccion = value; }

        public string Telefono { get => _telefono; set => _telefono = value; }


    }
}
