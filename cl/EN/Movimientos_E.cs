﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CLEntidad
{
    public class Movimientos_E
    {
        private int _id_movimiento;
        private string _nombre;

        public int Id_movimiento { get => _id_movimiento; set => _id_movimiento = value; }
        public string Nombre { get => _nombre; set => _nombre = value; }
    }
}
