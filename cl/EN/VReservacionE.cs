﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CLEntidad
{
    public class VReservacionE
    {
        private int _reservacion;
        private int _id_participante;
        private string _participante;
        private string _fechaentrada;
        private string _fechasalida;
        private string _hotel;
        private int _id_hotel;

        public int Reservacion { get => _reservacion; set => _reservacion = value; }
        public int Id_participante { get => _id_participante; set => _id_participante = value; }
        public string Participante { get => _participante; set => _participante = value; }
        public string Fechaentrada { get => _fechaentrada; set => _fechaentrada = value; }
        public string Fechasalida { get => _fechasalida; set => _fechasalida = value; }
        public string Hotel { get => _hotel; set => _hotel = value; }
        public int Id_hotel { get => _id_hotel; set => _id_hotel = value; }
    }
}
