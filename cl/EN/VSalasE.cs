﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CLEntidad
{
    public class VSalasE
    {
        private int _id_sala;
        private int _capacidad;
        private string _medios;
        private int _id_hotel;
        private string _nombre;

        public int Id_sala { get => _id_sala; set => _id_sala = value; }
        public int Capacidad { get => _capacidad; set => _capacidad = value; }
        public string Medios { get => _medios; set => _medios = value; }
        public int Id_hotel { get => _id_hotel; set => _id_hotel = value; }
        public string Nombre { get => _nombre; set => _nombre = value; }
    }
}
