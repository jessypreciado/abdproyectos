﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CLEntidad
{
    public class VPartida
    {
        private int _id;
        private int _partida;
        private string _movimiento;
        private int _id_sala;
        private string _arbitro;

        public int Id { get => _id; set => _id = value; }
        public int Partida { get => _partida; set => _partida = value; }
        public string Movimiento { get => _movimiento; set => _movimiento = value; }
        public int Id_sala { get => _id_sala; set => _id_sala = value; }
        public string Arbitro { get => _arbitro; set => _arbitro = value; }
    }
}
