﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CLEntidad
{
   public  class JuagadaE
    {

        private int _id_juagada;
        private string _color;
        private int _id_jugador;
        private int _id_partida;

        public int Id_juagada { get => _id_juagada; set => _id_juagada = value; }
        public string Color { get => _color; set => _color = value; }
        public int Id_jugador { get => _id_jugador; set => _id_jugador = value; }
        public int Id_partida { get => _id_partida; set => _id_partida = value; }
    }
}
