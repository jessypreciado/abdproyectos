﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CLEntidad
{
    public class VJugadasE
    {
        private int _id_jugada;
        private string _id_jugador;
        private string _color;
        private int _id_partida;

        public int Id_jugada { get => _id_jugada; set => _id_jugada = value; }
        public string Id_jugador { get => _id_jugador; set => _id_jugador = value; }
        public string Color { get => _color; set => _color = value; }
        public int Id_partida { get => _id_partida; set => _id_partida = value; }
    }
}
