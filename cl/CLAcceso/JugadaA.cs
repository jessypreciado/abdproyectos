﻿using System;
using System.Collections.Generic;
using System.Text;
using CLEntidad;
using System.Data;


namespace CLAcceso
{
    public class JugadaA
    {
        Conexion conexion;
        public JugadaA()
        {
            conexion = new Conexion("localhost", "root", "", "CLUB_AJEDREZ", 3306);
        }
        public void guardar(JuagadaE jugadaEntidad)// CREAR UN METODO PUBLICO DE GUARDAR PARA INSERTAR LOS VALORES DE LA TABLA , AGREGAMOS DOS CAMPOS MAS PARA INGRESAR LA CIUDAD Y EL ESTADO
        {
            if (jugadaEntidad.Id_juagada == 0)
            {
                string consulta = string.Format("call ProcedureJugadasInsertar(null,'{0}','{1}','{2}')", jugadaEntidad.Id_jugador, jugadaEntidad.Color, jugadaEntidad.Id_partida);//variables del get en may.
                conexion.EjecutarConsulta(consulta);
            }
            else //
            {

                string consulta = string.Format("call ProcedureJugadaActualizar('{0}', '{1}','{2}','{3}')", jugadaEntidad.Id_juagada, jugadaEntidad.Id_jugador, jugadaEntidad.Color, jugadaEntidad.Id_partida);
                conexion.EjecutarConsulta(consulta);
            }

        }
        public void eliminar(int pid_jugada)
        {
            string consulta = string.Format("call ProcedureJugadasEliminar('{0}')", pid_jugada);
            conexion.EjecutarConsulta(consulta);
        }
        public List<VJugadasE> GetVjugadas(string filtro)
        {
            var ListJugada = new List<VJugadasE>();
            var ds = new DataSet();
            String consulta = string.Format("call ProcedureJugadasMostrar('{0}')", filtro);
            ds = conexion.ObtenerDatos(consulta, "Vjugadas");
            var dt = new DataTable();

            dt = ds.Tables[0];//queremos la tabla 0

            foreach (DataRow row in dt.Rows)  // AGREGAR LAS COLUMNAS DE ESTADOS Y CIUDADES 
            {
                var VjugadaEntidad = new VJugadasE
                {
                    Id_jugada = Convert.ToInt32(row["JUGADA"]),
                    Id_jugador = row["JUGADOR"].ToString(),
                    Color = row["COLOR"].ToString(),
                    Id_partida = Convert.ToInt32(row["PARTIDA"])


                };
                ListJugada.Add(VjugadaEntidad);
            }
            return ListJugada;

        }

       
    }
}
