﻿using System;
using System.Collections.Generic;
using System.Text;
using CLAcceso;
using CLEntidad;
using System.Data;

namespace CLAcceso
{
    public class PartidaA
    {
        Conexion conexion;
        public PartidaA()
        {
            conexion = new Conexion("localhost", "root", "", "CLUB_AJEDREZ", 3306);
        }
        public void guardar(PartidasE partidasE)// CREAR UN METODO PUBLICO DE GUARDAR PARA INSERTAR LOS VALORES DE LA TABLA , AGREGAMOS DOS CAMPOS MAS PARA INGRESAR LA CIUDAD Y EL ESTADO
        {
            if (partidasE.Id == 0)
            {
                string consulta = string.Format("call ProcedurePartidaInsertar(null,'{0}','{1}','{2}','{3}')",  partidasE.Id_partida, partidasE.Id_movimiento,partidasE.Id_arbitro,partidasE.Id_sala);//variables del get en may.
                conexion.EjecutarConsulta(consulta);
            }
            else //
            {

                string consulta = string.Format("call ProcedurePartidaActualizar('{0}', '{1}','{2}','{3}','{4}')", partidasE.Id, partidasE.Id_partida, partidasE.Id_movimiento, partidasE.Id_arbitro, partidasE.Id_sala);
                conexion.EjecutarConsulta(consulta);
            }

        }
        public void eliminar(int id_sala)
        {
            string consulta = string.Format("call ProcedurePartidasEliminar('{0}')", id_sala);
            conexion.EjecutarConsulta(consulta);
        }
        public List<VPartida> GetVPartida(string filtro)
        {
            var ListSalas = new List<VPartida>();
            var ds = new DataSet();
            String consulta = string.Format("call ProcedurePartidasMostrar('{0}')", filtro);
            ds = conexion.ObtenerDatos(consulta, "VPartida");
            var dt = new DataTable();

            dt = ds.Tables[0];//queremos la tabla 0

            foreach (DataRow row in dt.Rows)  // AGREGAR LAS COLUMNAS DE ESTADOS Y CIUDADES 
            {
                var vpartida= new VPartida
                {
                    Id= Convert.ToInt32(row["ID"]),
                    Partida = Convert.ToInt32(row["PARTIDA"]),
                    Movimiento= row["MOVIMIENTO"].ToString(),
                    Id_sala = Convert.ToInt32(row["ID SALA"]),
                    Arbitro = row["ARBITRO"].ToString(),

                };
                ListSalas.Add(vpartida);
            }
            return ListSalas;

        }

        public DataSet Getidpartida(string filtro)
        {
            var da = new DataSet();
            string consulta = "select  id_partida from partidas where nombre like'%" + filtro + "%'";
            da = conexion.ObtenerDatos(consulta, "partida");
            return da;
        }


    }
}
