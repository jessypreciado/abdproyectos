﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using CLEntidad;

namespace CLAcceso
{

    public class VSalasA
    {
        Conexion conexion;
        public VSalasA()
        {
            conexion = new Conexion("localhost", "root", "", "CLUB_AJEDREZ", 3306);
        }
        public List<VSalasE> GetSalas(string filtro)
        {
            var ListSalas = new List<VSalasE>();
            var ds = new DataSet();
            String consulta = string.Format("call ProcedureSalasMostrar('{0}')", filtro);
            ds = conexion.ObtenerDatos(consulta, "Vsalas");
            var dt = new DataTable();

            dt = ds.Tables[0];//queremos la tabla 0

            foreach (DataRow row in dt.Rows)  // AGREGAR LAS COLUMNAS DE ESTADOS Y CIUDADES 
            {
                var salasEntidad = new VSalasE
                {
                    Id_sala = Convert.ToInt32(row["id_sala"]),
                    Capacidad = Convert.ToInt32(row["capacidad"]),
                    Medios = row["medios"].ToString(),
                    Id_hotel = Convert.ToInt32(row["id_hotel"]),
                    Nombre = row["nombre"].ToString()
                };
                ListSalas.Add(salasEntidad);
            }
            return ListSalas;

        }

    }
}
