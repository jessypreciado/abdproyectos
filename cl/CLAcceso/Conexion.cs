﻿using System;
using System.Collections.Generic;
using System.Text;

using MySql.Data.MySqlClient;
using System.Data;

namespace CLAcceso
{
    public class Conexion
    {
        private MySqlConnection conn;

        //modificar constructor(nombre de la clase) de esta clase para cuando se haga la instancia pasar los datos 
        public Conexion(string servidor, string usuario, string password, string database, uint puerto)//cotr y doble tabulador
        {
            //instancia de msql
            MySqlConnectionStringBuilder CadenaConexion = new MySqlConnectionStringBuilder();//OBJETO DE TIPO STRING BUILDER
            CadenaConexion.Server = servidor;
            CadenaConexion.UserID = usuario;
            CadenaConexion.Password = password;
            CadenaConexion.Database = database;
            CadenaConexion.Port = puerto;

            conn = new MySqlConnection(CadenaConexion.ToString());
        }
        public void EjecutarConsulta(string consulta)
        {
            conn.Open();
            var command = new MySqlCommand(consulta, conn);
            command.ExecuteNonQuery();
            conn.Close();

        }
        public DataSet ObtenerDatos(string consulta, string tabla)
        {
            var ds = new DataSet();

            MySqlDataAdapter da = new MySqlDataAdapter(consulta, conn);//llenar el dataset con el adaptador
           da.Fill(ds, tabla);
            return ds;

        }















    }
}
