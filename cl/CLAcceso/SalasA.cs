﻿using System;
using System.Collections.Generic;
using System.Text;
using CLAcceso;
using CLEntidad;
using System.Data;

namespace CLAcceso
{
    
   public  class SalasA
    {
        Conexion conexion;
        public SalasA()
        {
            conexion = new Conexion("localhost", "root", "", "CLUB_AJEDREZ", 3306);
        }
        public void guardar(Salas_E salasEntidad)// CREAR UN METODO PUBLICO DE GUARDAR PARA INSERTAR LOS VALORES DE LA TABLA , AGREGAMOS DOS CAMPOS MAS PARA INGRESAR LA CIUDAD Y EL ESTADO
        {
            if (salasEntidad.Id_sala == 0)
            {
                string consulta = string.Format("call ProcedureSalaInsertar(null,'{0}','{1}','{2}')", salasEntidad.Capacidad, salasEntidad.Medios, salasEntidad.Id_hotel);//variables del get en may.
                conexion.EjecutarConsulta(consulta);
            }
            else //
            {

                string consulta = string.Format("call ProcedureSalasActualizar('{0}', '{1}','{2}','{3}')", salasEntidad.Id_sala, salasEntidad.Capacidad, salasEntidad.Medios, salasEntidad.Id_hotel);
                conexion.EjecutarConsulta(consulta);
            }

        }
        public void eliminar(int id_sala)
        {
            string consulta = string.Format("call ProcedureSalasEliminar ('{0}')", id_sala);
            conexion.EjecutarConsulta(consulta);
        }
  

    }
}
