﻿using System;
using System.Collections.Generic;
using System.Text;
using CLEntidad;
using System.Data;

namespace CLAcceso
{
    public class ParticipantesA
    {
        Conexion conexion;
        public ParticipantesA()
        {
            conexion = new Conexion("localhost", "root", "", "club_Ajedrez", 3306);
        }
        public void guardar(ParticipantesE participantes)// CREAR UN METODO PUBLICO DE GUARDAR PARA INSERTAR LOS VALORES DE LA TABLA , AGREGAMOS DOS CAMPOS MAS PARA INGRESAR LA CIUDAD Y EL ESTADO
        {
            if (participantes.Id == 0)
            {
                string consulta = string.Format("call ProcedureParticipanteJugInsertar (null,'{0}','{1}','{2}','{3}','{4}','{5}','{6}')", participantes.Id_participante, participantes.Nombre, participantes.Direccion, participantes.Telefono, participantes.Tipo, participantes.Id_pais, participantes.Niveljuego);//variables del get en may.
                conexion.EjecutarConsulta(consulta);
            }
            else //
            {

                string consulta = string.Format("call ProcedureParticipanteJugActualizar('{0}','{1}','{2}','{3}','{4}','{5}','{6}')", participantes.Id_participante, participantes.Nombre, participantes.Direccion, participantes.Telefono, participantes.Tipo, participantes.Id_pais, participantes.Niveljuego);
                conexion.EjecutarConsulta(consulta);
            }


        }
        public void eliminar(string id_reservacion)
        {
            string consulta = string.Format("call ProcedureParticianteJugEliminar ('{0}')", id_reservacion);
            conexion.EjecutarConsulta(consulta);
        }
        public List<ParticipantesE> GetParticipantes(string filtro)
        {
            var ListReservacion = new List<ParticipantesE>();
            var ds = new DataSet();
            string consulta = "select *from participantes where nombre like'%" + filtro + "%'";
            ds = conexion.ObtenerDatos(consulta, "participantes");
            var dt = new DataTable();

            dt = ds.Tables[0];//queremos la tabla 0

            foreach (DataRow row in dt.Rows)  // AGREGAR LAS COLUMNAS DE ESTADOS Y CIUDADES 
            {
                var ParticipantesEntidad = new ParticipantesE
                {
                    Id = Convert.ToInt32(row["id"]),
                    Id_participante = row["id_participante"].ToString(),
                    Nombre = row["nombre"].ToString(),
                    Direccion = row["direccion"].ToString(),
                    Telefono = row["telefono"].ToString(),
                    Tipo = Convert.ToInt32(row["tipo"]),
                    Id_pais = Convert.ToInt32(row["id_pais"]),
                    Niveljuego = Convert.ToInt32(row["niveljuego"]),




                };
                ListReservacion.Add(ParticipantesEntidad);
            }
            return ListReservacion;

        }
        public DataSet GetidParticipantes(string filtro)
        {
            var da = new DataSet();
            string consulta = "select  id_participante from participantes where nombre like'%" + filtro + "%'";
            da = conexion.ObtenerDatos(consulta, "participantes");
            return da;
        }
        public List<JugadoresE> GetJugadores(string filtro)
        {
            var Listjugadores = new List<JugadoresE>();
            var ds = new DataSet();
            string consulta = string.Format("call ProcedureJugMostrar('{0}')", filtro);
            ds = conexion.ObtenerDatos(consulta, "Vjugadores");
            var dt = new DataTable();

            dt = ds.Tables[0];//queremos la tabla 0

            foreach (DataRow row in dt.Rows)  // AGREGAR LAS COLUMNAS DE ESTADOS Y CIUDADES 
            {
                var jugadoresE = new JugadoresE
                {
                    Id = Convert.ToInt32(row["id"]),
                    Id_participante = row["id_participante"].ToString(),
                    Nombre = row["nombre"].ToString(),
                    Direccion = row["direccion"].ToString(),
                    Telefono = row["telefono"].ToString(),
                   
                    Pais = row["pais"].ToString(),
                    Niveljuego = Convert.ToInt32(row["niveljuego"])

                };
                Listjugadores.Add(jugadoresE);
            }
            return Listjugadores;

        }
        public List<ArbitroE> GetArbitro(string filtro)
        {
            var ListReservacion = new List<ArbitroE>();
            var ds = new DataSet();
            string consulta = string.Format("call ProcedureParticipanteArbMostrar('{0}')", filtro);
            ds = conexion.ObtenerDatos(consulta, "VistaArbitro");
            var dt = new DataTable();

            dt = ds.Tables[0];//queremos la tabla 0

            foreach (DataRow row in dt.Rows)  // AGREGAR LAS COLUMNAS DE ESTADOS Y CIUDADES 
            {
                var arbitroE = new  ArbitroE
                {
                    Id = Convert.ToInt32(row["id"]),
                     Id_participante= row["id_participante"].ToString(),
                    Nombre = row["nombre"].ToString(),
                    Direccion = row["direccion"].ToString(),
                    Telefono = row["telefono"].ToString(),   
                   Pais= row["pais"].ToString()
       
                };
                ListReservacion.Add(arbitroE);
            }
            return ListReservacion;

        }

        public void guardararbitro(ParticipantesE participantes)// CREAR UN METODO PUBLICO DE GUARDAR PARA INSERTAR LOS VALORES DE LA TABLA , AGREGAMOS DOS CAMPOS MAS PARA INGRESAR LA CIUDAD Y EL ESTADO
        {
            if (participantes.Id == 0)
            {
                string consulta = string.Format("call ProcedureParticipanteArbInsertar (null,'{0}','{1}','{2}','{3}','{4}','{5}','{6}')", participantes.Id_participante, participantes.Nombre, participantes.Direccion, participantes.Telefono, participantes.Tipo, participantes.Id_pais, participantes.Niveljuego);//variables del get en may.
                conexion.EjecutarConsulta(consulta);
            }
            else //
            {

                string consulta = string.Format("call ProcedureParticipanteArbActualizar('{0}','{1}','{2}','{3}','{4}','{5}','{6}')", participantes.Id,participantes.Id_participante, participantes.Nombre, participantes.Direccion, participantes.Telefono, participantes.Tipo, participantes.Id_pais);
                conexion.EjecutarConsulta(consulta);
            }


        }
        public void eliminarArbitro(string id_arbitro)
        {
            string consulta = string.Format("call ProcedureParticianteArbEliminar ('{0}')", id_arbitro);
            conexion.EjecutarConsulta(consulta);
        }
    }
}


