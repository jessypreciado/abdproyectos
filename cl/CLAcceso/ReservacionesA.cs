﻿using System;
using System.Collections.Generic;
using System.Text;
using CLEntidad;
using System.Data;


namespace CLAcceso
{
    public class ReservacionesA
    {
        Conexion conexion;

        public ReservacionesA()
        {
            conexion = new Conexion("localhost", "root", "", "club_Ajedrez", 3306);
        }

        public void guardar(ReservacionesE reservacionEntidad)// CREAR UN METODO PUBLICO DE GUARDAR PARA INSERTAR LOS VALORES DE LA TABLA , AGREGAMOS DOS CAMPOS MAS PARA INGRESAR LA CIUDAD Y EL ESTADO
        {
            if (reservacionEntidad.Id_reservacion == 0)
            {
                string consulta = string.Format("call ProcedureReservacionesInsertar(null,'{0}','{1}','{2}','{3}')", reservacionEntidad.Fechaentrada, reservacionEntidad.Fechasalida, reservacionEntidad.Id_hotel, reservacionEntidad.Idparticipante);//variables del get en may.
                conexion.EjecutarConsulta(consulta);
            }
            else //
            {

                string consulta = string.Format("call ProcedurReservacionActualizar('{0}', '{1}','{2}','{3}','{4}')", reservacionEntidad.Id_reservacion, reservacionEntidad.Fechaentrada, reservacionEntidad.Fechasalida, reservacionEntidad.Id_hotel, reservacionEntidad.Idparticipante);
                conexion.EjecutarConsulta(consulta);
            }


        }
        public void eliminar(int id_reservacion)
        {
            string consulta = string.Format("call ProcedureReservacionesEliminar ('{0}')", id_reservacion);
            conexion.EjecutarConsulta(consulta);
        }
        public List<ReservacionesE> GetReservacions(string filtro)
        {
            var ListReservacion = new List<ReservacionesE>();
            var ds = new DataSet();
            string consulta = "select *from reservaciones where fecha_entrada like'%" + filtro + "%'";
            ds = conexion.ObtenerDatos(consulta, "reservaciones");
            var dt = new DataTable();

            dt = ds.Tables[0];//queremos la tabla 0

            foreach (DataRow row in dt.Rows)  // AGREGAR LAS COLUMNAS DE ESTADOS Y CIUDADES 
            {
                var ReservacionEntidad = new ReservacionesE
                {
                    Id_reservacion = Convert.ToInt32(row["id_reservacion"]),
                    Id_hotel = Convert.ToInt32(row["id_hotel"]),
                    Idparticipante = row["id_participante"].ToString(),
                    Fechaentrada = row["fecha_entrada"].ToString(),
                    Fechasalida = row["fecha_salida"].ToString(),
                   
                };
                ListReservacion.Add(ReservacionEntidad);
            }
            return ListReservacion;

        }


    }
}
