﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using CLEntidad;



namespace CLAcceso
{
    public class HotelA
    {

        Conexion conexion;
        public HotelA()
        {
            conexion = new Conexion("localhost", "root", "", "CLUB_AJEDREZ", 3306);
        }

        public void guardar(Hotel_E hotelesEntidad)// CREAR UN METODO PUBLICO DE GUARDAR PARA INSERTAR LOS VALORES DE LA TABLA , AGREGAMOS DOS CAMPOS MAS PARA INGRESAR LA CIUDAD Y EL ESTADO
        {
            if (hotelesEntidad.Id_hotel == 0)
            {
                string consulta = string.Format("call ProceduresHotelesInsertar(null,'{0}','{1}','{2}')", hotelesEntidad.Nombre, hotelesEntidad.Direccion, hotelesEntidad.Telefono);//variables del get en may.
                conexion.EjecutarConsulta(consulta);
            }
            else //
            {

                string consulta = string.Format("call ProcedureHotelesActualizar('{0}', '{1}','{2}','{3}')", hotelesEntidad.Id_hotel, hotelesEntidad.Nombre, hotelesEntidad.Direccion, hotelesEntidad.Telefono);
                conexion.EjecutarConsulta(consulta);
            }

        }
        public void eliminar(int id_hotel)
        {
            string consulta = string.Format("call ProcedureHotelesEliminar('{0}')", id_hotel);
            conexion.EjecutarConsulta(consulta);
        }
        public List<Hotel_E> GetHoteles(string filtro)
        {
            var ListHoteles = new List<Hotel_E>();
            var ds = new DataSet();
            string consulta = "select *from hoteles where nombre like'%" + filtro + "%'";
            ds = conexion.ObtenerDatos(consulta, "hoteles");
            var dt = new DataTable();

            dt = ds.Tables[0];//queremos la tabla 0

            foreach (DataRow row in dt.Rows)  // AGREGAR LAS COLUMNAS DE ESTADOS Y CIUDADES 
            {
                var hotelesEntidad = new Hotel_E
                {
                    Id_hotel = Convert.ToInt32(row["id_hotel"]),
                    Nombre = row["nombre"].ToString(),
                    Direccion = row["direccion"].ToString(),
                    Telefono = row["telefono"].ToString(),
                };
                ListHoteles.Add(hotelesEntidad);
            }
            return ListHoteles;

        }
        public DataSet GetidHotel(string filtro)
        {
            var da = new DataSet();
            string consulta = "select  id_hotel from hoteles where nombre like'%" + filtro + "%'";
            da = conexion.ObtenerDatos(consulta, "hoteles");
            return da;
        }



    }
}
