﻿using System;
using System.Collections.Generic;
using System.Text;
using CLEntidad;
using System.Data;

namespace CLAcceso
{
    public class Movimientos_A
    {
        Conexion conexion;

        public Movimientos_A()
        {
            conexion = new Conexion("localhost", "root", "", "CLUB_AJEDREZ", 3306);
        }
        public void guardar(Movimientos_E movimentos)// CREAR UN METODO PUBLICO DE GUARDAR PARA INSERTAR LOS VALORES DE LA TABLA , AGREGAMOS DOS CAMPOS MAS PARA INGRESAR LA CIUDAD Y EL ESTADO
        {
            if (movimentos.Id_movimiento == 0)
            {
                string consulta = string.Format("call ProcedureMovimientosInsertar (null,'{0}')", movimentos.Nombre);//variables del get en may.
                conexion.EjecutarConsulta(consulta);
            }
            else //
            {

                string consulta = string.Format("call proceduremovimientosActualizar('{0}', '{1}')", movimentos.Id_movimiento, movimentos.Nombre);
                conexion.EjecutarConsulta(consulta);
            }
      
        }
        public void eliminar(int id_movimiento)
        {
            string consulta = string.Format("call ProceduremovimientosEliminar ('{0}')", id_movimiento);
            conexion.EjecutarConsulta(consulta);
        }

        public List<Movimientos_E> GetMovimientos(string filtro)
        {
            var ListMovimientos = new List<Movimientos_E>();
            var ds = new DataSet();
            string consulta = "select *from movimientos where nombre like'%" + filtro + "%'";
            ds = conexion.ObtenerDatos(consulta, "movimientos");
            var dt = new DataTable();

            dt = ds.Tables[0];//queremos la tabla 0

            foreach (DataRow row in dt.Rows)  // AGREGAR LAS COLUMNAS DE ESTADOS Y CIUDADES 
            {
                var movimentosEntidad = new Movimientos_E
                {
                    Id_movimiento = Convert.ToInt32(row["id_movimiento"]),
                    Nombre = row["nombre"].ToString(),
                };
                ListMovimientos.Add(movimentosEntidad);
            }
            return ListMovimientos;

        }
        public DataSet Getidmov(string filtro)
        {
            var da = new DataSet();
            string consulta = "select  id_movimiento from movimientos where nombre like'%" + filtro + "%'";
            da = conexion.ObtenerDatos(consulta, "movimientos");
            return da;
        }
    }
}
