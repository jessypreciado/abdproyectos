﻿using System;
using System.Collections.Generic;
using System.Text;
using CLEntidad;
using System.Data;
namespace CLAcceso
{
    public class VReservacionA
    {
        Conexion conexion;
        public VReservacionA()
        {
            conexion = new Conexion("localhost", "root", "", "CLUB_AJEDREZ", 3306);
        }
        public List<VReservacionE> GetReserv(string filtro)
        {
            var ListSalas = new List<VReservacionE>();
            var ds = new DataSet();
            String consulta = string.Format("call ProcedureReservacionMostrar('{0}')", filtro);
            ds = conexion.ObtenerDatos(consulta, "vreservacion");
            var dt = new DataTable();

            dt = ds.Tables[0];//queremos la tabla 0

            foreach (DataRow row in dt.Rows)  // AGREGAR LAS COLUMNAS DE ESTADOS Y CIUDADES 
            {
                var salasEntidad = new VReservacionE
                {
                    Reservacion= Convert.ToInt32(row["RESERVACION"]),
                    Id_participante= Convert.ToInt32(row["id_participante"]),
                    Participante = row["participante"].ToString(),
                   Fechaentrada = row["FEHA ENTRADA"].ToString(),
                    Fechasalida = row["FECHA SALIDA"].ToString(),
                   Hotel= row["HOTEL"].ToString(),
                    Id_hotel = Convert.ToInt32(row["id_hotel"]),


                };
                ListSalas.Add(salasEntidad);
            }
            return ListSalas;

        }
    }
}
