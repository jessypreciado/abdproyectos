﻿using System;
using System.Collections.Generic;
using System.Text;
using CLAcceso;
using CLEntidad;
using System.Data;
namespace CLAcceso
{
    public class PaisesA
    {
        Conexion conexion;

        public PaisesA()
        {
            conexion = new Conexion("localhost", "root", "", "CLUB_AJEDREZ", 3306);
        }

        public void guardar(PaisE   paisesEntidad)// CREAR UN METODO PUBLICO DE GUARDAR PARA INSERTAR LOS VALORES DE LA TABLA , AGREGAMOS DOS CAMPOS MAS PARA INGRESAR LA CIUDAD Y EL ESTADO
        {
            if (paisesEntidad.Idpais == 0)
            {
                string consulta = string.Format("call ProcedurePaisInsertar (null,'{0}','{1}','{2}')", paisesEntidad.Nombre, paisesEntidad.Cantclub, paisesEntidad.Id_paisr);//variables del get en may.
                conexion.EjecutarConsulta(consulta);
            }
            else //
            {

                string consulta = string.Format("call ProcedurePaisActualizar('{0}', '{1}', '{2}','{3}')", paisesEntidad.Idpais, paisesEntidad.Nombre, paisesEntidad.Cantclub, paisesEntidad.Id_paisr);
                conexion.EjecutarConsulta(consulta);
            }

        }
        public void eliminar(int id_pais)
        {
            string consulta = string.Format("call ProcedurePaisEliminar ('{0}')", id_pais);
            conexion.EjecutarConsulta(consulta);
        }
        public List<PaisE> GetPaises(string filtro)
        {
            var ListPaises = new List<PaisE>();
            var ds = new DataSet();
            string consulta = "select *from paises where nombre like'%" + filtro + "%'";
            ds = conexion.ObtenerDatos(consulta, "paises");
            var dt = new DataTable();

            dt = ds.Tables[0];//queremos la tabla 0

            foreach (DataRow row in dt.Rows)  // AGREGAR LAS COLUMNAS DE ESTADOS Y CIUDADES 
            {
                var paisesEntidad = new  PaisE
                {
                    Idpais = Convert.ToInt32(row["id_pais"]),
                    Nombre = row["nombre"].ToString(),
                    Cantclub = Convert.ToInt32(row["cantclub"]),
                    Id_paisr = row["id_paisr"].ToString()

                };
                ListPaises.Add(paisesEntidad);
            }
            return ListPaises;

        }
        public DataSet GetidPaises(string filtro)
        {
            var da = new DataSet();
            string consulta = "select  id_pais from paises where nombre like'%" + filtro + "%'";
            da = conexion.ObtenerDatos(consulta, "paises");
            return da;
        }


    }
}
