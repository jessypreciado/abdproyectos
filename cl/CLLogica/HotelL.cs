﻿using System;
using System.Collections.Generic;
using System.Text;
using CLAcceso;
using CLEntidad;
using System.Data;



namespace CLLogica
{
    public class HotelL
    {
        private HotelA _hotelesAccesoDatos = new HotelA();
        public void guardar(Hotel_E hotelesEntidad)
        {
            _hotelesAccesoDatos.guardar(hotelesEntidad);

        }
        public void eliminar(int id)
        {
            _hotelesAccesoDatos.eliminar(id);

        }
        public List<Hotel_E> GetHoteles(string filtro)
        {
            var ListHot = _hotelesAccesoDatos.GetHoteles(filtro);
            return ListHot;
        }
        public DataSet GetidHotel(string filtro)
        {
            var ds = _hotelesAccesoDatos.GetidHotel(filtro);
            return ds;
        }





    }
}
