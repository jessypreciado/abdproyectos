﻿using System;
using System.Collections.Generic;
using System.Text;
using CLEntidad;
using CLAcceso;
using System.Data;
namespace CLLogica
{
    public class PaisL
    {
        private PaisesA _paisesAccesoDatos = new PaisesA();
        public void guardar(PaisE paisesEntidad)
        {
            _paisesAccesoDatos.guardar(paisesEntidad);

        }
        public void eliminar(int id)
        {
            _paisesAccesoDatos.eliminar(id);

        }
        public List<PaisE> GetPaises(string filtro)
        {
            var ListPa = _paisesAccesoDatos.GetPaises(filtro);
            return ListPa;
        }
        public DataSet GetidPaises(string filtro)
        {
            var ds = _paisesAccesoDatos.GetidPaises(filtro);
            return ds;
        }
    }
}
