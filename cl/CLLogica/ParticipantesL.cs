﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;

using CLAcceso;
using CLEntidad;

namespace CLLogica
{
    public class ParticipantesL
    {
        private ParticipantesA _participantesAccesoDatos = new ParticipantesA();
        public List<ParticipantesE> GetParticipantes(string filtro)
        {
            var ListArb = _participantesAccesoDatos.GetParticipantes(filtro);
            return ListArb;
        }
        public DataSet GetiParticipantes(string filtro)
        {
            var ds = _participantesAccesoDatos.GetidParticipantes(filtro);
            return ds;
        }

        public void guardar(ParticipantesE participantesEntidad)
        {
           _participantesAccesoDatos.guardar(participantesEntidad);

        }
        public void eliminar(string id)
        {
            _participantesAccesoDatos.eliminar(id);

        }
        public List<JugadoresE> Getjugadores(string filtro)
        {
            var Listjug = _participantesAccesoDatos.GetJugadores(filtro);
            return Listjug;
        }
        public List<ArbitroE> GetArbitro(string filtro)
        {
            var Listjug = _participantesAccesoDatos.GetArbitro(filtro);
            return Listjug;
        }

        public void guardarArbitro(ParticipantesE participantesE)
        {
           _participantesAccesoDatos.guardararbitro(participantesE);

        }
        public void eliminarArbitro(string id)
        {
            _participantesAccesoDatos.eliminarArbitro(id);

        }
        public DataSet Getidpart(string filtro)
        {
            var ds = _participantesAccesoDatos.GetidParticipantes(filtro);
            return ds;
        }
        

    }
}
