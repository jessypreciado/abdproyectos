﻿using System;
using System.Collections.Generic;
using System.Text;
using CLEntidad;
using CLAcceso;

namespace CLLogica
{
    public class RservacionesL
    {
        private ReservacionesA _ReservacionessAccesoDatos = new ReservacionesA();

        public void guardar(ReservacionesE  reservacionEntidad)
        {
            _ReservacionessAccesoDatos.guardar(reservacionEntidad);

        }
        public void eliminar(int id)
        {
            _ReservacionessAccesoDatos.eliminar(id);

        }
        public List<ReservacionesE> GetReservaciones(string filtro)
        {
            var ListReservaciones = _ReservacionessAccesoDatos.GetReservacions(filtro);
            return ListReservaciones;
        }
    }
}
