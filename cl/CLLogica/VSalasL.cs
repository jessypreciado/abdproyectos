﻿using System;
using System.Collections.Generic;
using System.Text;
using CLAcceso;
using CLEntidad;
using System.Data;

namespace CLLogica
{
    public class VSalasL
    {
        private VSalasA _salasAccesoDatos = new VSalasA();
        public List<VSalasE> GetSalas(string filtro)
        {
            var ListSal = _salasAccesoDatos.GetSalas(filtro);
            return ListSal;
        }
    }
}
