﻿using System;
using System.Collections.Generic;
using System.Text;
using CLAcceso;
using CLEntidad;
using System.Data;

namespace CLLogica
{
    public class Movimiento_L
    {
        private Movimientos_A _movimientosAccesoDatos = new Movimientos_A();

        public void guardar(Movimientos_E movimentosEntidad)
        {
            _movimientosAccesoDatos.guardar(movimentosEntidad);

        }
        public void eliminar(int id)
        {
            _movimientosAccesoDatos.eliminar(id);

        }
        public List<Movimientos_E> Getmovimientos(string filtro)
        {
            var Listmov = _movimientosAccesoDatos.GetMovimientos(filtro);
            return Listmov;
        }
        public DataSet Getidmov(string filtro)
        {
            var ds = _movimientosAccesoDatos.Getidmov(filtro);
            return ds;
        }

    }
}
