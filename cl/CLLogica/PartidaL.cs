﻿using System;
using System.Collections.Generic;
using System.Text;
using CLAcceso;
using CLEntidad;
using System.Data;


namespace CLLogica
{
    public class PartidaL
    {
        private PartidaA _partida = new PartidaA();
        public void guardar(PartidasE partidaEntidad)
        {
            _partida.guardar(partidaEntidad);

        }
        public void eliminar(int id)
        {
            _partida.eliminar(id);

        }
        public List<VPartida> GetVpart(string filtro)
        {
            var Listpar = _partida.GetVPartida(filtro);
            return Listpar;
        }
        public DataSet Getidpartida(string filtro)
        {
            var ds = _partida.Getidpartida(filtro);
            return ds;
        }



    }
}

